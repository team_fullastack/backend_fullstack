# Backend (trivioServer)

The `trivioServer` is the backend component of the Trivio quiz application. It is built using Spring Boot and provides RESTful APIs for managing users, quizzes, messages, and other resources. Backend is split into config, controller, dto, exception, model, repository and service folders. 

## Content
- [Background](#Background)
- [ER-Diagram](#er-diagram)
- [Functionality](#functionality)
- [Plugins and technologies](#technologies-and-plugins)
- [CI/CD](#continious-integration-and-continious-deployment)
- [Security](#security)
- [Installation](#installation)
- [Documentation](#documentation)
- [Tests](#tests)
- [Future work](#future-work)

## Background
This is a voluntary project in the subject IDATT2105, where
we had to create a quiz-bank. This repository focuses on the backend of the product, where its job is to store and deploy entities inside a database. This database is responsible for storing users, trivios, questions etc, including authenticate users.

## ER-Diagram
<img src="src/main/resources/docs/ER_Trivio.png" width="500" height="500">

## Functionality
### User
* Can log in
* Sign up and create another user
* Create a trivio
* Edit a trivio
* Delete a trivio
* Invite other users to collaborate on a trivio
* Change password and user information

### Trivio
* Is playable
* Can be added images
* Can be assigned to a category
* Filterable
* Submit Score
### Questions
* Assign tags
* Includes question types like True or false or multiple choice

## Technologies and plugins
* Java
* Spring Boot
* Spring Data JPA
* Spring Security
* H2 Database
* JWT Token

## Continious Integration and Continious Deployment
Continious Integration (CI) / Continious Deployment is performed by Gitlab runners. These pipelines are configurated in a .gitlab-ci.yml.file where we define the jobs. 

In the pipelines, there are three stages that we check. 
* The first is to check if the server builds
* Secondly we run all the tests
* Lastly, we deploy and package the program into a .jar file.

CI / CD are currently only available in backend.

## Security
For authentication, Trivio requires JWT-tokens for authentication of users that calls to the API. This token is given to the user everytime they are logged in. For the security configuration in Trivio, we have used a JWTFilterChain that intercepts API-calls and checks whether the user has a valid token or not. If user has a token, they will be authorized, else every API-call will be forbidden.

In this project we decided to store our secret key that is used to sign the token inside the application.properties file. While this is not good practice, because the file is public. We think this it is a flexible solution in the future to use .gitignore and have multiple application.properties that are for different environments. e.g. Development, staging, producton. 

## Installation
#### Prerequisites
* Maven
* Java

### Instruction Manual 1

1. Make sure you have Java Development Kit (JDK) version 17 or higher installed.

2. Clone the repository:
```bash
git clone https://gitlab.stud.idi.ntnu.no/team_fullastack/backend_fullstack.git
```

3. Navigate to directory:
```bash
cd backend_fullstack
```

4. Build and run the project with Maven:
```bash
  mvn:spring-boot:run
```
## Prerequisites
* Docker
### Instruction Manual 2

1. Make sure you have Docker installed and it is running

2. Run Make-script for building docker image
```bash
    make build
```

3. Run Make-script for starting up a container
```bash
    make run
```
## Tests 
For tests we have used mockito for creating unit tests, and we have used jacico for generating test coverage 
The test coverage can be found after executing the following command:

```bash
mvn clean test
```
The user can find the test coverage in the target files after running clean test locally 

path: /target/site/jacico/index.html

## Documentation 
#### Swagger:
To get Swagger documentation, you have to run the program locally, and go to 
```bash
http://localhost:8080/swagger-ui/index.html
```

JavaDoc:  

1. Navigate to the directory containing project

2. Execute following command:
```bash
 mvn javadoc:javadoc
```
The following javadoc can be found inside the target file the same way as it is for the jacico test-coverage.

path: /target/site/apidocs/index.html
## Database
The current database we are using is H2 database. Our database configuration is H2 inmemory database, more details can be found inside application properties.

In the initiation of backend, test data are created instantly. The initiation happens in LoadDatabaseConfig file.

There are currently two users that are already defined in the database in the initialization. 

`user 1: "username": "test", "password": "password"`<br/>

`user 2: "username": "test2, "password": "password"`
## Future work
### Implementing administration 
The problem with the current program is that there is no administrative methods that are used. Currently, the only admin methohd is an api-call for getting all users and trivios, but for now, they are not used in prod, but rather for testing.  

### Datestamps in trivios
As for now, we only have the timestamps and date for submitting the score. In the future, we think it is important to also include dates in when a trivio is created, and when it was last modified.

### Better way to store secret key
Our current method for storing the key is inside the application.properties file. For now, it is really not good practice, cause the key is out in public. In the future we could implement other methods to store the secret key. Either in differnt application.propeterties file or databases or env.files. 


### Different approach than an inmemory database
As we mentioned previously, we used h2 inmemory database, which meant by design that it is volatile and results in dataloss after application start. This can be changed in behavior by using a file-based storage in h2. 

## Authors
Gia Hy Nguyen  
Tini Tran  
Vilde Min Vikan  
