# Use the official OpenJDK image as a base
FROM openjdk:21-jdk

# Set the working directory inside the container
WORKDIR /app

# Copy the packaged JAR file into the container
COPY target/trivioServer-0.0.1-SNAPSHOT.jar /app/trivio-backend.jar

# Copy the static resources (images) into the Docker container
COPY src/main/resources/public/images /app/src/main/resources/public/images

EXPOSE 8080

# Specify the command to run your Spring Boot application
CMD ["java", "-jar", "trivio-backend.jar"]
