run:
	docker run --rm -p 8080:8080 trivio-backend

build:
	mvn clean package
	docker build -t trivio-backend .

