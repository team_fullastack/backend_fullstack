package ntnu.idatt2105.group44.trivioServer.repository;

import java.util.Optional;
import ntnu.idatt2105.group44.trivioServer.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

  Optional<User> findUserByUsername(String username);

  Optional<User> findUserByEmail(String email);
}
