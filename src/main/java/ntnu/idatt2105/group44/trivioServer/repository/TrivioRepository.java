package ntnu.idatt2105.group44.trivioServer.repository;

import java.util.List;
import java.util.Optional;

import ntnu.idatt2105.group44.trivioServer.dto.TrivioWithQAndA;
import ntnu.idatt2105.group44.trivioServer.model.Trivio;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TrivioRepository extends JpaRepository<Trivio, Long>, JpaSpecificationExecutor<Trivio> {
  Optional<Trivio> findTrivioByTitle(String title);

  List<Trivio> findTrivioByVisibility(String visibility);

  List<Trivio> findTrivioByUserId(long userId);
  List<Trivio> findTrivioByVisibilityAndUserIdNot(String visibility, long userId);

  Page<Trivio> findAll(Specification<Trivio> specification, Pageable pageable);

}
