package ntnu.idatt2105.group44.trivioServer.repository;

import java.util.Optional;
import ntnu.idatt2105.group44.trivioServer.model.Result;
import ntnu.idatt2105.group44.trivioServer.model.Trivio;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultRepository extends JpaRepository<Result, Long> {

  Page<Result> getResultsByUserId(Long userId, Pageable pageable);

  Page<Result> getResultsByUserIdAndTrivio_Title(Long userId, String title, Pageable pageable);

  @Query("SELECT DISTINCT t.title FROM Result r " + "JOIN r.trivio t")
  List<String> findDistinctTrivioTitlesByUserId(Long userId);
}
