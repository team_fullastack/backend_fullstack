package ntnu.idatt2105.group44.trivioServer.repository;

import ntnu.idatt2105.group44.trivioServer.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> findBySenderName(String senderName);
}