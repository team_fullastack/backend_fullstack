package ntnu.idatt2105.group44.trivioServer.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import ntnu.idatt2105.group44.trivioServer.model.Question;
import ntnu.idatt2105.group44.trivioServer.model.Trivio;
import ntnu.idatt2105.group44.trivioServer.service.QuestionService;
import ntnu.idatt2105.group44.trivioServer.service.TrivioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@Tag(name = "Trivio Question Controller", description = "Endpoints to retrieve questions for trivios."
    + "This end point is mostly used for testing")
@RestController
@RequestMapping("/trivios")
public class TrivioQuestionController {
  private TrivioService trivioService;
  private QuestionService questionService;

  @Autowired
  public TrivioQuestionController(TrivioService trivioService, QuestionService questionService) {
    this.trivioService = trivioService;
    this.questionService = questionService;
  }

  /**
   * GET-method to retrieve questions for a trivio.
   *
   * @param trivioId the ID of the trivio to retrieve questions for.
   * @return a response entity with the list of questions for the trivio.
   */
  @Operation(
      summary = "Get questions for a trivio",
      description = "Retrieve questions for a trivio by providing its ID.")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Questions retrieved successfully")
  })
  @GetMapping("{trivioId}/questions")
  public ResponseEntity<List<Question>> getQuestionForTrivia(@PathVariable Long trivioId){
    Trivio trivio  = trivioService.getTrivioById(trivioId);
    System.out.println(trivio.getQuestionList());
    System.out.println(trivio.getQuestionList().size());

    return ResponseEntity.ok(trivio.getQuestionList());
  }

}
