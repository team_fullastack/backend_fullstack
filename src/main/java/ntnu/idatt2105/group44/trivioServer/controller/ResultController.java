package ntnu.idatt2105.group44.trivioServer.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import ntnu.idatt2105.group44.trivioServer.dto.ResultDTO;
import ntnu.idatt2105.group44.trivioServer.model.Result;
import ntnu.idatt2105.group44.trivioServer.repository.ResultRepository;
import ntnu.idatt2105.group44.trivioServer.service.JWTService;
import ntnu.idatt2105.group44.trivioServer.service.ResultService;
import ntnu.idatt2105.group44.trivioServer.service.storage.LocalFileStorageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
@Tag(name = "Messages", description = "Results is the endpoint where the user can retrieve and submit"
    + "their attemps in trivios")
@RestController
@RequestMapping("results")
public class ResultController {
  private final ResultService resultService;
  private final JWTService jwtService;
  private static final Logger logger = LogManager.getLogger(LocalFileStorageService.class);

  public ResultController(ResultService resultService, JWTService jwtService){
    this.resultService = resultService;
    this.jwtService = jwtService;
  }

  /**
   * GET-method to fetch attempts from a chosen user within a chosen trivio.
   *
   * @param token    token to authenticate and identify the user.
   * @param title    title of the trivio to filter results by.
   * @param pageable pageable object for pagination.
   * @return a response entity with a page of results or bad request status.
   */
  @Operation(
      summary = "Fetches attempts from a chosen user within a chosen trivio",
      description = "Get results by user ID and optional title."
  )
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Successful operation"),
      @ApiResponse(responseCode = "400", description = "Bad request")
  })
  @GetMapping()
  public ResponseEntity<Page<Result>> getResultByUserIdAndTitle(
      @RequestHeader("Authorization") String token,
      @RequestParam(required = false) String title,
      Pageable pageable) {
    try {
      long userId = Long.parseLong(jwtService.extractSubjectFromHeader(token));
      Page<Result> results;

      if (title != null) {
        results = resultService.getResultByUserIdAndTitle(userId, title, pageable);
      } else {
        results = resultService.getResultByUserId(userId, pageable);
      }
      logger.info("Successfully received results");
      return ResponseEntity.ok(results);
    } catch (RuntimeException e) {
      return ResponseEntity.badRequest().build();
    }
  }
  /**
   * GET-method to fetch distinct trivio titles associated with the user.
   *
   * @param token token to authenticate and identify the user.
   * @return a response entity with a list of distinct trivio titles or bad request status.
   */
  @Operation(
      summary = "Get distinct trivio titles",
      description = "Fetches distinct trivio titles associated with the user."
  )
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Fetches results"),
      @ApiResponse(responseCode = "400", description = "Bad request")
  })
  @GetMapping("/trivioTitles")
  public ResponseEntity<List<String>> getDistinctTrivioTitles(@RequestHeader("Authorization") String token) {
    long userId = Long.parseLong(jwtService.extractSubjectFromHeader(token));
    List<String> trivioTitles = resultService.getDistinctTrivioTitles(userId);
    return ResponseEntity.ok(trivioTitles);
  }

   /**
   * POST-method to add a result to the server.
   *
   * @param resultDTO the result to be added.
   * @return a response entity with a success message or bad request status.
   */
  @Operation(
      summary = "Add a result",
      description = "Posts a result to the server."
  )
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Result is posted to the database"),
      @ApiResponse(responseCode = "400", description = "Bad request")
  })
  @PostMapping()
  public ResponseEntity<String> postResult(@RequestBody ResultDTO resultDTO) {
    resultService.addResult(resultDTO);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
