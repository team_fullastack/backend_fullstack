package ntnu.idatt2105.group44.trivioServer.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import java.util.logging.Logger;
import ntnu.idatt2105.group44.trivioServer.model.User;
import ntnu.idatt2105.group44.trivioServer.dto.UserResponse;
import ntnu.idatt2105.group44.trivioServer.dto.LoginRequest;
import ntnu.idatt2105.group44.trivioServer.service.JWTService;
import ntnu.idatt2105.group44.trivioServer.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "User Controller", description = "Endpoints to manage users")
@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserController {
  private static final Logger logger = Logger.getLogger(UserController.class.getName());

  private final UserService userService;
  private final JWTService jwtService;

  @Autowired
  public UserController(UserService userService, JWTService jwtService) {
    this.userService = userService;
    this.jwtService = jwtService;
  }
  /**
   * GET-method to retrieve all users.
   *
   * @return a list of all users.
   */
  @Operation(
      summary = "Get all users",
      description = "Retrieve all users registered in the system. Only administrators can access this endpoint.")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Users retrieved successfully")
  })
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/all")
  public List<User> getUsers() {
    return userService.getAllUsers();
  }

  /**
   * GET-method to retrieve a user by ID.
   *
   * @param userId ID of the user to retrieve.
   * @return the user with the specified ID.
   */
  @Operation(
      summary = "Get user by ID",
      description = "Retrieve a user by their ID. Only administrators can access this endpoint.")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "User retrieved successfully")
  })
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping(path = "{userId}")
  public User getUserById(@Parameter(description = "ID of the user to retrieve", required = true) @PathVariable Long userId) {
    return userService.getUserById(userId);
  }

  /**
   * GET-method to retrieve information of the authenticated user.
   *
   * @param header Authorization header containing the JWT token.
   * @return the authenticated user's information.
   */
  @Operation(
      summary = "Get user information",
      description = "Retrieve information of the authenticated user.")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "User information retrieved successfully")
  })
  @GetMapping()
  public User getUserInfo(@RequestHeader("Authorization") String header) {
    return userService.getUserById(Long.valueOf(jwtService.extractSubjectFromHeader(header)));
  }

  /**
   * PUT-method to update user information (username and email).
   *
   * @param header   Authorization header containing the JWT token.
   * @param username New username (optional).
   * @param email    New email (optional).
   * @return a response indicating the success or failure of the operation.
   */
  @Operation(
      summary = "Update user information",
      description = "Update user information (username and email).")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Username or email successfully changed"),
      @ApiResponse(responseCode = "400", description = "Failed to update user information")
  })
  @PutMapping("/edit")
  public ResponseEntity<String> updateUserInfo(
      @RequestHeader("Authorization") String header,
      @Parameter(description = "New username (optional)") @RequestParam(required = false) String username,
      @Parameter(description = "New email (optional)") @RequestParam(required = false) String email) {
    try {
      String userId = jwtService.extractSubjectFromHeader(header);
      userService.updateUser(username, email, Long.valueOf(userId));
      logger.info("Username and email for user " + userId + " is changed");
      return ResponseEntity.ok("Username or email successfully changed");
    } catch (RuntimeException err) {
      return ResponseEntity.badRequest().body(err.getMessage());
    }
  }

  /**
   * PUT-method to update user password.
   *
   * @param header   Authorization header containing the JWT token.
   * @param password New password.
   * @return a response indicating the success or failure of the operation.
   */
  @Operation(
      summary = "Update user password",
      description = "Update user password.")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Password has successfully been changed"),
      @ApiResponse(responseCode = "400", description = "Bad request")
  })
  @PutMapping("/editPassword")
  public ResponseEntity<String> updatePassword(@RequestHeader("Authorization") String header,
      @RequestParam String password){
    try {
      String userId = jwtService.extractSubjectFromHeader(header);
      userService.updatePassword(password, Long.valueOf(userId));
      logger.info("Password for user " + userId + " is changed");
      return ResponseEntity.ok("Password has successfully been changed!");
    } catch (RuntimeException err) {
      return  ResponseEntity.badRequest().body(err.getMessage());
    }

  }

}
