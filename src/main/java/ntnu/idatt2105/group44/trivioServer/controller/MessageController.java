package ntnu.idatt2105.group44.trivioServer.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import ntnu.idatt2105.group44.trivioServer.service.storage.LocalFileStorageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ntnu.idatt2105.group44.trivioServer.model.Message;
import ntnu.idatt2105.group44.trivioServer.dto.UserResponse;
import ntnu.idatt2105.group44.trivioServer.service.MessageService;
import java.util.List;
@Tag(name = "Messages", description = "THis API is responsible for messages")
@CrossOrigin
@RestController
@RequestMapping("/messages")
public class MessageController {

    private final MessageService messageService;
    private static final Logger logger = LogManager.getLogger(MessageController.class);

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }
    /**
     * POST-method to save a message in the database.
     *
     * @param message the message to be saved.
     * @return a response entity with a success message or bad request status.
     */
    @Operation(
        summary = "Saves message",
        description = "Saves message in the database")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "201", description = "Messsage is created")
        , @ApiResponse(responseCode = "400", description = "Failed to create message")
    })
    @PostMapping
    public ResponseEntity<UserResponse> createMessage(@RequestBody Message message) {
        try {
            messageService.createMessage(message);
            logger.info("Message sent!");
            return ResponseEntity.ok(new UserResponse("Message sent. Thanks for feedback!"));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new UserResponse("Failed to send message. Please try again later."));
        }
    }
    /**
     * GET-method to fetch messages from the database.
     *
     * @return a response entity with a list of messages or bad request status.
     */
    @Operation(
        summary = "Fetch messages",
        description = "Fetches messages from the database")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "201", description = "Messages is retrieved")
    })
    @GetMapping
    public ResponseEntity<List<Message>> getAllMessages() {
        List<Message> messages = messageService.getAllMessages();
        return ResponseEntity.ok(messages);
    }
}
