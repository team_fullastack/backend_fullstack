package ntnu.idatt2105.group44.trivioServer.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import ntnu.idatt2105.group44.trivioServer.config.JWTAuthorizationFilter;
import ntnu.idatt2105.group44.trivioServer.dto.LoginRequest;
import ntnu.idatt2105.group44.trivioServer.dto.SignUpRequest;
import ntnu.idatt2105.group44.trivioServer.exception.UserAlreadyExistException;
import ntnu.idatt2105.group44.trivioServer.model.User;
import ntnu.idatt2105.group44.trivioServer.service.AuthenticationService;
import ntnu.idatt2105.group44.trivioServer.service.JWTService;
import ntnu.idatt2105.group44.trivioServer.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Authentication Controller", description = "These endpoints is responsible for "
    + "logging in and signing up")
@RestController
@RequestMapping
@CrossOrigin
public class AuthController {

  private AuthenticationService authenticationService;
  private JWTService jwtService;

  private UserService userService;

  private static final Logger LOGGER = LogManager.getLogger(AuthController.class);


  @Autowired
  public AuthController(AuthenticationService authenticationService, JWTService jwtService, UserService userService){
    this.authenticationService = authenticationService;
    this.jwtService = jwtService;
    this.userService = userService;
  }
  @Operation(
      summary = "Authenticates the user and generates a token",
      description = "The endpoints takes a login request and authenticates the credentials,"
          + "if the credentials are valid, a token is generated")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Successful, token is generated and returned")
      , @ApiResponse(responseCode = "401", description = "The credentials are invalid")
  })
  @PostMapping("/login")
  public ResponseEntity<String> login(@RequestBody LoginRequest loginRequest){
    if(authenticationService.authenticateUser(loginRequest)){
      User user = userService.getUserByUsername(loginRequest.getUsername());
      LOGGER.info("User logged in: "+user.getUsername());
      return new ResponseEntity<>(jwtService.generateToken(Long.toString(user.getId())), HttpStatus.OK);
    } else return new ResponseEntity<>("Invalid Credentials!", HttpStatus.UNAUTHORIZED);
  }
  @Operation(
      summary = "Creates an user",
      description = "Creates an user and saves it in the database")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "User is created")
      , @ApiResponse(responseCode = "400", description = "User credentials already exists in database")
  })
  @PostMapping("/signup")
  public ResponseEntity<String> signup(@RequestBody SignUpRequest signUpRequest){
    try{
      if(!authenticationService.credentialsExists(signUpRequest)){
        userService.createUser(signUpRequest);
        LOGGER.info("user created!");
        return new ResponseEntity<>("Successfully created user!", HttpStatus.CREATED);
      } else {
        throw new UserAlreadyExistException("This user already exists!");
      }
    } catch (UserAlreadyExistException err) {
      return new ResponseEntity<>(err.getMessage(), HttpStatus.UNAUTHORIZED);
    }


  }
}
