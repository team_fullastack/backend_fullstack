package ntnu.idatt2105.group44.trivioServer.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import ntnu.idatt2105.group44.trivioServer.dto.TrivioWithQAndA;
import ntnu.idatt2105.group44.trivioServer.model.Trivio;
import ntnu.idatt2105.group44.trivioServer.service.JWTService;
import ntnu.idatt2105.group44.trivioServer.service.TrivioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/trivios")
@Tag(name = "Trivio", description = "Endpoints for managing Trivios")
public class TrivioController {
  private final TrivioService trivioService;
  private final JWTService jwtService;
  private static final Logger logger = Logger.getLogger(TrivioController.class.getName());

  @Autowired
  public TrivioController(TrivioService trivioService, JWTService jwtService){
    this.trivioService=trivioService;
    this.jwtService=jwtService;
  }
  @Operation(summary = "Get all Trivios",
      description = "Retrieves all Trivios."
  )
  @ApiResponses(
      @ApiResponse(responseCode = "200", description = "Retrieved all trivios")
  )
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping
  public List<Trivio> getAllTrivio(){
  return trivioService.getAllTrivios();
  }




  /**
   * GET-method to retrieve all trivios by a specific user with filters if necessary.
   * @param token token to authenticate and identify the user.
   * @param category category for filtering the trivios.
   * @param difficulty difficulty for filtering the trivios.
   * @param tagString tags for filtering the trivios.
   * @param pageable page of trivios to retrieve.
   * @return page with filtered trivios by the specific users.
   */
  @Operation(summary = "Get filtered Trivios by user",
      description = "Retrieves filtered Trivios by a specific user.")
  @ApiResponse(responseCode = "200", description = "Retrieved filtered trivios")
  @GetMapping("/user")
  public ResponseEntity<Page<Trivio>> getFilteredTriviosByUser(
      @Parameter(in = ParameterIn.HEADER, description = "Authorization token", required = true)
      @RequestHeader("Authorization") String token,
          @RequestParam(required = false) String category,
          @RequestParam(required = false) String difficulty,
          @RequestParam(required = false) String tagString,
          Pageable pageable) {

    List<String> tags = null;
    if (tagString != null) {
      tags = Arrays.asList(tagString.split(","));
      logger.info(tags.toString());
    }

    long userId = Long.parseLong(jwtService.extractSubjectFromHeader(token));
    Page<Trivio> trivios = trivioService.getFilteredTriviosByUser(
            userId,category, difficulty, tags, pageable);
    logger.info("Received trivios!");
    return new ResponseEntity<>(trivios, HttpStatus.OK);
  }

  /**
   * GET-method to retrieve all public trivios by other users with filters if necessary.
   * @param token token to authenticate and identify the user.
   * @param category category for filtering the trivios.
   * @param difficulty difficulty for filtering the trivios.
   * @param tagString tags for filtering the trivios.
   * @param pageable page of trivios to retrieve.
   * @return page with filtered public trivios by other users.
   */
  @Operation(summary = "Get filtered public Trivios by other users",
      description = "Retrieves filtered public Trivios by other users.")
  @ApiResponse(responseCode = "200", description = "Retrieved filtered public trivios")
  @GetMapping("/discover")
  public ResponseEntity<Page<Trivio>> getFilteredPublicTriviosByOtherUsers(
      @Parameter(in = ParameterIn.HEADER, description = "Authorization token", required = true)
      @RequestHeader("Authorization") String token,
          @RequestParam(required = false) String category,
          @RequestParam(required = false) String difficulty,
          @RequestParam(required = false) String tagString,
          Pageable pageable) {

    long userId = Long.parseLong(jwtService.extractSubjectFromHeader(token));
    String visibility = "public";
    logger.info(tagString);
    logger.info(category);

    List<String> tags = null;
    if (tagString != null) {
      tags = Arrays.asList(tagString.split(","));
      logger.info(tags.toString());
    }
    logger.info("Successfully received trivios that are public!");
    Page<Trivio> trivios = trivioService.getFilteredPublicTriviosByOtherUsers(
            userId,category, difficulty, tags, visibility,pageable);
    return new ResponseEntity<>(trivios, HttpStatus.OK);
  }

  /**
   * GET-method to retrieve shared trivios by the user with filters if necessary.
   * @param token token to authenticate and identify the user.
   * @param category category for filtering the trivios.
   * @param difficulty difficulty for filtering the trivios.
   * @param tagString tags for filtering the trivios.
   * @param pageable page of trivios to retrieve.
   * @return page with filtered shared trivios by the user.
   */
  @Operation(summary = "Get shared Trivios",
      description = "Retrieves shared Trivios by the user.")
  @ApiResponse(responseCode = "200", description = "Retrieved shared trivios")
  @GetMapping("/shared")
  public ResponseEntity<Page<Trivio>> getSharedTrivios(
      @Parameter(in = ParameterIn.HEADER, description = "Authorization token", required = true)
      @RequestHeader("Authorization") String token,
      @RequestParam(required = false) String category,
      @RequestParam(required = false) String difficulty,
      @RequestParam(required = false) String tagString,
      Pageable pageable) {

    try {
      long userId = Long.parseLong(jwtService.extractSubjectFromHeader(token));
      logger.info(tagString);
      logger.info(category);

      List<String> tags = null;
      if (tagString != null) {
        tags = Arrays.asList(tagString.split(","));
        logger.info(tags.toString());
      }

      Page<Trivio> trivios = trivioService.getFilteredSharedTriviosByUser(
          userId,category,difficulty, tags,pageable);
      logger.info("Successfully retrieved shared trivio");
      return new ResponseEntity<>(trivios, HttpStatus.OK);
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }

  /**
   * GET-method to retrieve a trivio by its ID.
   * @param token token to authenticate and identify the user.
   * @param trivioId ID of the trivio to retrieve.
   * @return the trivio if authorized, otherwise returns unauthorized status.
   */
  @Operation(summary = "Get Trivio by ID",
      description = "Retrieves a Trivio by its ID.")
  @ApiResponse(responseCode = "200", description = "Retrieved trivio")
  @ApiResponse(responseCode = "401", description = "Unauthorized")
  @GetMapping("/{trivioId}")
  public ResponseEntity<Trivio> getTrivio(
      @Parameter(in = ParameterIn.HEADER, description = "Authorization token", required = true)
      @RequestHeader("Authorization") String token,
      @Parameter(description = "Trivio ID", required = true)
      @PathVariable Long trivioId) {
    try {
      long userId = Long.parseLong(jwtService.extractSubjectFromHeader(token));
      Trivio trivio = trivioService.getTrivioById(trivioId);

      if (trivio.user.getId() != userId && !trivio.visibility.equals("public")) {
        throw new RuntimeException("You are not allowed to retrieve this trivio!");
      } else {
        return ResponseEntity.ok(trivio);
      }
    } catch (Exception e){
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
  }

  /**
   * POST-method to create a new trivio with questions and answers.
   * @param token token to authenticate and identify the user.
   * @param trivioWithQuestionsAndAnswers TrivioWithQAndA object containing trivio details.
   * @return a response entity with a success message or unauthorized status.
   */
  @Operation(summary = "Create Trivio with Questions and Answers",
      description = "Creates a new Trivio with associated questions and answers.")
  @ApiResponse(responseCode = "200", description = "Trivio Created")
  @ApiResponse(responseCode = "401", description = "Unauthorized user")
  @PostMapping("/create")
  public ResponseEntity<String> addTrivioWithQuestionsAndAnswers(
      @Parameter(in = ParameterIn.HEADER, description = "Authorization token", required = true)
      @RequestHeader("Authorization") String token,
      @Parameter(description = "Trivio with Questions and Answers", required = true)
      @RequestBody TrivioWithQAndA trivioWithQuestionsAndAnswers) {
    try {
      if (token == null || !token.startsWith("Bearer ")) {
        // Token is missing or invalid format
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
      }
      String userId = jwtService.extractSubjectFromHeader(token);
      trivioService.createTrivio(trivioWithQuestionsAndAnswers, Long.parseLong(userId));
      return ResponseEntity.ok("Trivio Created!");
    } catch (Exception e){
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
    }
  }

  /**
   * PUT-method to edit a trivio with questions and answers.
   * @param token token to authenticate and identify the user.
   * @param updatedTrivioWithQuestionsAndAnswer Updated TrivioWithQAndA object containing edited trivio details.
   * @param trivioId ID of the trivio to be edited.
   * @return a response entity with a success message or unauthorized status.
   */
  @Operation(summary = "Edit Trivio with Questions and Answers",
      description = "Edits an existing Trivio with associated questions and answers.")
  @ApiResponse(responseCode = "200", description = "Trivio Updated")
  @ApiResponse(responseCode = "400", description = "Bad Request")
  @ApiResponse(responseCode = "401", description = "Unauthorized")
  @PutMapping("/edit/{trivioId}")
  public ResponseEntity<String> editTrivioWithQuestionsAndAnswers(
      @Parameter(in = ParameterIn.HEADER, description = "Authorization token", required = true)
      @RequestHeader("Authorization") String token,
      @Parameter(description = "Updated Trivio with Questions and Answers", required = true)
      @RequestBody TrivioWithQAndA updatedTrivioWithQuestionsAndAnswer,
      @Parameter(description = "ID of the Trivio to be edited", required = true)
      @PathVariable long trivioId) {
    try {
      // Extract user ID from the JWT token
      long userId = Long.parseLong(jwtService.extractSubjectFromHeader(token));
      // Retrieve the trivio by its ID
      Trivio trivio = trivioService.getTrivioById(trivioId);
      // Check if the user is authorized to edit the trivio
      if (trivio.user.getId() == userId || trivioService.isUserInListCanEdit(trivio, userId)) {
        // Update the trivio using the service
        trivioService.editTrivio(updatedTrivioWithQuestionsAndAnswer, trivioId);
        // Return a success response
        return ResponseEntity.ok("Trivio Updated!");
      } else {
        // If the user is not authorized, log and return an unauthorized response
        logger.severe("Could not update trivio: User is not authorized");
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
      }
    } catch (Exception e) {
      // Log the exception
      logger.info("An error occurred while updating trivio");
      logger.info(e.getMessage());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("An error occurred while updating trivio: " + e.getMessage());
    }
  }

  /**
   * PUT-method to add a user to a list that gives them rights to edit a trivio.
   * @param token token to authenticate and identify the user.
   * @param username username of the user to be added.
   * @param trivioId ID of the trivio.
   * @return a response entity with a success message or bad request status.
   */
  @Operation(summary = "Add User That Can Edit",
      description = "Adds a user that can edit a trivio.")
  @ApiResponse(responseCode = "200", description = "User added")
  @ApiResponse(responseCode = "400", description = "Bad Request")
  @PutMapping("/add-user")
  public ResponseEntity<String> addUserThatCanEdit(
      @Parameter(in = ParameterIn.HEADER, description = "Authorization token", required = true)
      @RequestHeader("Authorization") String token,
      @Parameter(description = "Username of the user to be added", required = true)
      @RequestParam String username,
      @Parameter(description = "ID of the trivio", required = true)
      @RequestParam long trivioId) {
    try {
      long userId = Long.parseLong(jwtService.extractSubjectFromHeader(token));
      Trivio trivio = trivioService.getTrivioById(trivioId);
      logger.info(Long.toString(trivio.getUser().getId()));
      logger.info(Long.toString(userId));
      if (userId == trivio.user.getId()) {
        trivioService.addUserToTrivio(trivioId, username);
        return ResponseEntity.ok("User added!");
      }
      logger.info("User not authorized");
      return null;
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("An error occurred while adding an user: " + e.getMessage());
    }
  }

  /**
   * PUT-method to remove a user that can edit a trivio.
   * @param token token to authenticate and identify the user.
   * @param username username of the user to be removed.
   * @param trivioId ID of the trivio.
   * @return a response entity with a success message or unauthorized status.
   */
  @Operation(summary = "Remove User That Can Edit",
      description = "Removes a user that can edit a trivio.")
  @ApiResponse(responseCode = "200", description = "User removed")
  @ApiResponse(responseCode = "401", description = "Unauthorized")
  @ApiResponse(responseCode = "400", description = "Bad Request")
  @PutMapping("/remove-user")
  public ResponseEntity<String> removeUserThatCanEdit(
      @Parameter(in = ParameterIn.HEADER, description = "Authorization token", required = true)
      @RequestHeader("Authorization") String token,
      @Parameter(description = "Username of the user to be removed", required = true)
      @RequestParam String username,
      @Parameter(description = "ID of the trivio", required = true)
      @RequestParam long trivioId) {
    try {
      long userId = Long.parseLong(jwtService.extractSubjectFromHeader(token));
      Trivio trivio = trivioService.getTrivioById(trivioId);
      if(userId == trivio.getUser().getId()){
        trivioService.removeUserFromTrivioEditorList(trivioId, username);
        return ResponseEntity.ok("User removed!");
      }
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("User is not authorized to perform this action.");
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("An error occurred while removing the user: " + e.getMessage());
    }
  }
  /**
   * DELETE-method to delete a trivio.
   * @param token token to authenticate and identify the user.
   * @param trivioId ID of the trivio to be deleted.
   * @return a response entity with a success message or unauthorized status.
   */
  @Operation(summary = "Delete Trivio",
      description = "Deletes a trivio by its ID.")
  @ApiResponse(responseCode = "200", description = "Trivio successfully removed")
  @ApiResponse(responseCode = "401", description = "Unauthorized")
  @ApiResponse(responseCode = "400", description = "Bad Request")
  @DeleteMapping("/delete/{trivioId}")
  public ResponseEntity<String> deleteTrivio(
      @Parameter(in = ParameterIn.HEADER, description = "Authorization token", required = true)
      @RequestHeader("Authorization") String token,
      @Parameter(description = "ID of the trivio to be deleted", required = true)
      @PathVariable Long trivioId) {
    try {
      if (trivioService.checkIfUserIsOwner(token, trivioId)) {
        trivioService.deleteTrivio(trivioId);
        return ResponseEntity.ok("Trivio successfully removed!");
      }
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("User is not authorized!");
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("An error occurred while deleting trivio: " + e.getMessage());
    }
  }
}
