package ntnu.idatt2105.group44.trivioServer.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.io.FileNotFoundException;
import ntnu.idatt2105.group44.trivioServer.config.JWTAuthorizationFilter;
import ntnu.idatt2105.group44.trivioServer.service.storage.LocalFileStorageService;
import ntnu.idatt2105.group44.trivioServer.service.storage.StorageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
@Tag(name = "FileUpload Controller", description = "These endpoints handle images and stores them"
    + "locally in the resource directory")
@RestController
@CrossOrigin
public class FileUploadController {

  private final StorageService storageService;
  private static final Logger logger = LogManager.getLogger(LocalFileStorageService.class);

  @Autowired
  public FileUploadController(StorageService storageService) {
    this.storageService = storageService;
  }
  /**
   * POST-method to upload a file.
   *
   * @param file the file to be uploaded.
   * @return a response entity with the file path or bad request status if the file cannot be uploaded.
   */
  @Operation(
      summary = "Uploads a file",
      description = "Takes in a file and copies it into the resource directory")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "File is uploaded into the directory"),
      @ApiResponse(responseCode = "400", description = "File is null or unable to be uploaded")
  })
  @PostMapping("/upload")
  public ResponseEntity<String> handleFileUpload(
      @RequestBody MultipartFile file) {
    try {
      String filePath = storageService.saveFile(file);
      logger.info("File uploaded: "+filePath);
      return ResponseEntity.ok(filePath);
    } catch (Exception e) {
      logger.atError().log("Failed to upload file: "+e.getMessage());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST)
          .body("Failed to upload file: " + e.getMessage());
    }
  }
}

