package ntnu.idatt2105.group44.trivioServer.advice;

import ntnu.idatt2105.group44.trivioServer.exception.TrivioNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class TrivioNotFoundAdvice {

  @ExceptionHandler
  @ResponseStatus(HttpStatus.NOT_FOUND)
  String TrivioNotFoundHandler(TrivioNotFoundException ex) {
    return ex.getMessage();
  }
}
