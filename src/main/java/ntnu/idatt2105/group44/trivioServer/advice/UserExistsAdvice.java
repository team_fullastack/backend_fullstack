package ntnu.idatt2105.group44.trivioServer.advice;

import ntnu.idatt2105.group44.trivioServer.exception.UserAlreadyExistException;
import ntnu.idatt2105.group44.trivioServer.exception.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class UserExistsAdvice {

  @ExceptionHandler(UserAlreadyExistException.class)
  @ResponseStatus(HttpStatus.CONFLICT)
  String UserExistsHandler(UserAlreadyExistException ex){
    return ex.getMessage();
  }

}
