package ntnu.idatt2105.group44.trivioServer.dto;

public class ResultDTO {
  private String username;

  private int score;

  private Long trivioId;

  public ResultDTO(String username, int score, Long trivioId) {
    this.username = username;
    this.score = score;
    this.trivioId = trivioId;
  }

  public String getUsername() {
    return username;
  }

  public int getScore() {
    return score;
  }

  public Long getTrivioId() {
    return trivioId;
  }
}
