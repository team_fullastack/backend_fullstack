package ntnu.idatt2105.group44.trivioServer.dto;

import java.util.List;
import ntnu.idatt2105.group44.trivioServer.model.Trivio;

public class TrivioWithQAndA {

  private Trivio trivio;
  private List<QuestionWithAnswers> questionsWithAnswers;

  private Long userId;

  public Trivio getTrivio() {
    return trivio;
  }

  public void setTrivio(Trivio trivio) {
    this.trivio = trivio;
  }

  public Long getUserId() {
    return userId;
  }

  public List<QuestionWithAnswers> getQuestionsWithAnswers() {
    return questionsWithAnswers;
  }

  public void setQuestionsWithAnswers(List<QuestionWithAnswers> questionsWithAnswers) {
    this.questionsWithAnswers = questionsWithAnswers;
  }
}
