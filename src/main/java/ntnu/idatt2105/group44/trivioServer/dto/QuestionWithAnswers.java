package ntnu.idatt2105.group44.trivioServer.dto;

import java.util.List;
import ntnu.idatt2105.group44.trivioServer.model.Answer;
import ntnu.idatt2105.group44.trivioServer.model.Question;
//selve DTO-klasse for å abstraktere spørsmål- og svar-entitetene
public class QuestionWithAnswers {
  private Question question;
  private List<Answer> answers;

  public Question getQuestion() {
    return question;
  }

  public void setQuestion(Question question) {
    this.question = question;
  }

  public List<Answer> getAnswers() {
    return answers;
  }

  public void setAnswers(List<Answer> answers) {
    this.answers = answers;
  }
}


