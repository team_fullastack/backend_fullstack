package ntnu.idatt2105.group44.trivioServer.dto;

public class UserDTO {
  private Long id;
  private String username;
  private String email;

  public Long getId() {
    return id;
  }

  public String getUsername() {
    return username;
  }

  public String getEmail() {
    return email;
  }
}
