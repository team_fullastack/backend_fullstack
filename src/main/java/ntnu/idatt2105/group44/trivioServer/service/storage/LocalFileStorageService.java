package ntnu.idatt2105.group44.trivioServer.service.storage;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import ntnu.idatt2105.group44.trivioServer.exception.StorageException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class LocalFileStorageService implements StorageService{
  @Value("${picture.path}")
  private String uploadDirectory;

  /**
   * Saves the provided file to storage and returns the URL or path where it is stored.
   *
   * @param file the file to save
   * @return the URL or path of the saved file
   * @throws StorageException if an error occurs during file storage
   */
  @Override
  public String saveFile(MultipartFile file) throws StorageException {
    try {
      Path uploadPath = Paths.get(uploadDirectory).toAbsolutePath();
      Files.createDirectories(uploadPath);
      if (file.getOriginalFilename() == null) {
        throw new StorageException("Original filename is null");
      }
      String fileName = StringUtils.cleanPath(file.getOriginalFilename());

      // Trim spaces from the filename
      String trimmedFileName = fileName.replaceAll("\\s", "");

      Path targetLocation = uploadPath.resolve(trimmedFileName);
      Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
      System.out.println(trimmedFileName);
      return trimmedFileName;
    } catch (IOException ex) {
      throw new StorageException("Failed to store file", ex);
    }
  }


  /**
   * Retrieves the file stored at the specified URL or path.
   *
   * @param fileUrl the URL or path of the file to retrieve
   * @return the file as a Resource
   * @throws FileNotFoundException if the file is not found
   */
  @Override
  public Resource getFile(String fileUrl) throws FileNotFoundException {
    try {
      Path filePath = Paths.get(uploadDirectory).resolve(fileUrl).normalize();
      Resource resource = new UrlResource(filePath.toUri());
      if (resource.exists() || resource.isReadable()) {
        return resource;
      } else {
        throw new FileNotFoundException("File not found: " + fileUrl);
      }
    } catch (MalformedURLException ex) {
      throw new FileNotFoundException("Invalid file URL: " + fileUrl);
    }
  }

//  /**
//   * Deletes the file stored at the specified URL or path.
//   *
//   * @param fileUrl the URL or path of the file to delete
//   * @throws FileNotFoundException if the file is not found
//   * @throws StorageException      if an error occurs during file deletion
//   */
//  @Override
//  public void deleteFile(String fileUrl) throws FileNotFoundException, StorageException {
//    try {
//      Path filePath = Paths.get(uploadDirectory).resolve(fileUrl).normalize();
//      Resource resource = new UrlResource(filePath.toUri());
//      if (!resource.exists() || !resource.isReadable()) {
//        throw new FileNotFoundException("File not found or is not readable: " + fileUrl);
//      }
//      Files.delete(filePath);
//    } catch (IOException ex) {
//      throw new StorageException("Failed to delete file: " + fileUrl, ex);
//    }
//  }

  public void setUploadDirectory(String uploadDirectory) {
    this.uploadDirectory = uploadDirectory;
  }
}
