package ntnu.idatt2105.group44.trivioServer.service;

import java.util.List;
import lombok.RequiredArgsConstructor;
import ntnu.idatt2105.group44.trivioServer.model.Answer;
import ntnu.idatt2105.group44.trivioServer.model.Question;
import ntnu.idatt2105.group44.trivioServer.repository.AnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AnswerService {
  private AnswerRepository answerRepository;


  public AnswerService(AnswerRepository answerRepository){
    this.answerRepository=answerRepository;
  }

  /**
   * Updates existing answers associated with a given question based on the provided lists of new and old answers.
   * Each pair of new and old answers is processed, updating the answer data of the existing answer.
   * Additionally, handles adding new answers and removing excess old answers as needed.
   *
   * @param newAnswers A list of {@link Answer} objects representing the new answers.
   * @param oldAnswers A list of {@link Answer} objects representing the existing answers.
   * @param question   The {@link Question} object associated with the answers.
   */
  @Transactional
  public void updateAnswers(List<Answer> newAnswers, List<Answer> oldAnswers, Question question) {
    int answerMinSize = Math.min(newAnswers.size(), oldAnswers.size());

    // Update answer data
    for (int j = 0; j < answerMinSize; j++) {
      Answer newAnswer = newAnswers.get(j);
      Answer oldAnswer = oldAnswers.get(j);
      oldAnswer.setAnswer(newAnswer.getAnswer());
      oldAnswer.setCorrect(newAnswer.isCorrect());
      answerRepository.save(oldAnswer);
    }
    // Handle adding new answers
    if (newAnswers.size() > oldAnswers.size()) {
      for (int j = answerMinSize; j < newAnswers.size(); j++) {
        Answer newAnswer = newAnswers.get(j);
        newAnswer.setQuestion(question);
        answerRepository.save(newAnswer);
      }
    }
    // Handle removing excess old answers
    if (newAnswers.size() < oldAnswers.size()) {
      for (int j = answerMinSize; j < oldAnswers.size(); j++) {
        answerRepository.delete(oldAnswers.get(j));
      }
    }
  }

}
