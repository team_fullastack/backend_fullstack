package ntnu.idatt2105.group44.trivioServer.service;

import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import ntnu.idatt2105.group44.trivioServer.dto.SignUpRequest;
import ntnu.idatt2105.group44.trivioServer.exception.UserAlreadyExistException;
import ntnu.idatt2105.group44.trivioServer.exception.UserNotFoundException;
import ntnu.idatt2105.group44.trivioServer.model.Role;
import ntnu.idatt2105.group44.trivioServer.model.User;
import ntnu.idatt2105.group44.trivioServer.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class UserService {
  UserRepository userRepository;

  @Value("${jwt.secret}")
  private String key;
  @Autowired
  public UserService(UserRepository userRepository){
    this.userRepository = userRepository;
  }

  /**
   * Method to get a user by its user-id from the user repository.
   *
   * @param id the id of the user.
   * @return the user from the repository or not-found exception if user is not found.
   */
  public User getUserById(Long id){
    Optional<User> userOptional = userRepository.findById(id);
    return userOptional.orElseThrow(() -> new UserNotFoundException(id));
  }

  /**
   * Method to get a user by its username from the user repository.
   *
   * @param username the username of the user.
   * @return the user from the repository or not-found exception if user is not found.
   */
  public User getUserByUsername(String username){
    Optional<User> userOptional = userRepository.findUserByUsername(username);
    return userOptional.orElseThrow(() -> new UserNotFoundException(username));
  }

  /**
   * Method to get all users from the user repository.
   *
   * @return all users in the user repository.
   */
  public List<User> getAllUsers(){
    return userRepository.findAll();
  }

  /**
   * Method to create a user from a signup-request.
   *
   * @param signUpRequest the signup-request containing user information.
   */
  public void createUser(SignUpRequest signUpRequest){
    if(signUpRequest.getUsername() != null && signUpRequest.getEmail() != null && signUpRequest.getPassword() != null){
      User user = new User.Builder()
          .username(signUpRequest.getUsername())
          .email(signUpRequest.getEmail())
          .password(signUpRequest.getPassword())
          .role(Role.USER)
          .build();
      userRepository.save(user);
    }
  }

  /**
   * Method to update an existing user with new information.
   *
   * @param username the new username.
   * @param email the new email.
   * @param userId the user-id for the user that is updated.
   */
  @Transactional
  public void updateUser(String username, String email, Long userId){
    User user = getUserById(userId);
    if (username != null && username.length() > 0 && !Objects.equals(username, user.getUsername())) {
        checkAndUpdateUsername(username, user);
    }if (email != null && email.length() > 0 && !Objects.equals(email, user.getEmail())) {
        checkAndUpdateEmail(email, user);
    }
    userRepository.save(user);
  }


  /**
   * Method to check if a username exists in the user repository,
   * and if not present, update a user with the username.
   *
   * @param newUsername the new username value.
   * @param user the username of the user to be updated.
   */
  private void checkAndUpdateUsername(String newUsername, User user) {
    if (checkIfUsernameIsPresent(newUsername)) {
      throw new UserAlreadyExistException("Username already exists!");
    }

    user.setUsername(newUsername);
  }

  /**
   * Method to check if an email exists in the user repository,
   * and if not present, update a user with the username.
   *
   * @param newEmail the new email value.
   * @param user the username of the user to be updated.
   */
  private void checkAndUpdateEmail(String newEmail, User user) {
    if (checkIfEmailIsPresent(newEmail)) {
      throw new UserAlreadyExistException("Email is already taken!");
    }
    user.setEmail(newEmail);
  }

  /**
   * Method to update an existing user with a new password. The password cannot be blank.
   *
   * @param password the new password value.
   * @param userId the user-id for the user to be updated.
   */
  @Transactional
  public void updatePassword(String password, Long userId){
    User user = getUserById(userId);
    if (password != null && password.length() > 0 && !Objects.equals(password, user.getPassword())) {
      user.setPassword(password);
    }
    userRepository.save(user);
  }

  /**
   * Method to check if a username is present in the user repository.
   *
   * @param name the username.
   * @return true-false statement for if the username is present.
   */
  public boolean checkIfUsernameIsPresent(String name){
    return userRepository.findUserByUsername(name).isPresent();
  }

  /**
   * Method to check if an email is present in the user repository,
   *
   * @param email the email
   * @return true-false statement for if the email is present.
   */
  public boolean checkIfEmailIsPresent(String email){
    return userRepository.findUserByEmail(email).isPresent();
  }
}
