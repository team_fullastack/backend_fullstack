package ntnu.idatt2105.group44.trivioServer.service;

import jakarta.persistence.criteria.*;
import ntnu.idatt2105.group44.trivioServer.model.Question;
import ntnu.idatt2105.group44.trivioServer.model.Trivio;
import org.springframework.data.jpa.domain.Specification;
import java.util.List;


/**
 * Class containing trivio-specifications for retrieving more
 * detailed and customized queries.
 */
public class TrivioSpecifications {

    //Method to specify filter trivios by user-id
    public static Specification<Trivio> filterByUserId(Long userId) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("user").get("id"), userId);
    }

    //Method to filter out user-id
    public static Specification<Trivio> filterByNotUserId(Long userId) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.notEqual(root.get("user").get("id"), userId);
    }

    // Method to filter trivios by user being in the shared list
    public static Specification<Trivio> filterByUserInSharedList(Long userId) {
        return (root, query, criteriaBuilder) -> {
            // Join the Trivio entity with the usersThatCanEdit collection
            Join<Object, Object> usersJoin = root.join("usersThatCanEdit", JoinType.INNER);

            // Create a predicate to check if the user ID matches
            Predicate userInSharedListPredicate = criteriaBuilder.equal(usersJoin.get("id"), userId);

            // Return the predicate
            return userInSharedListPredicate;
        };
    }

    //Method to filter trivios by visibility
    public static Specification<Trivio> filterByVisibility(String visibility){
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(criteriaBuilder.lower(root.get("visibility")), visibility.toLowerCase());
    }

    // Method to filter trivios by category
    public static Specification<Trivio> filterByCategory(String category) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(criteriaBuilder.lower(root.get("category")), category.toLowerCase());
    }

    // Method to filter trivios by difficulty
    public static Specification<Trivio> filterByDifficulty(String difficulty) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(criteriaBuilder.lower(root.get("difficulty")), difficulty.toLowerCase());
    }

    // Method to filter trivios by tags
    public static Specification<Trivio> filterByTags(List<String> tags) {
        return (root, query, criteriaBuilder) -> {
            // Join the Trivio entity with the Question entity
            Join<Trivio, Question> questionJoin = root.join("questionList", JoinType.INNER);

            // Create a list to hold tag predicates for each tag
            Predicate[] tagPredicates = new Predicate[tags.size()];

            // Iterate over each tag
            for (int i = 0; i < tags.size(); i++) {
                String tag = tags.get(i);
                // Construct a sub-query to check if any question in the trivio contains the tag
                Subquery<Long> subquery = query.subquery(Long.class);
                Root<Question> subQuestionRoot = subquery.from(Question.class);
                subquery.select(criteriaBuilder.count(subQuestionRoot));
                subquery.where(criteriaBuilder.and(
                        criteriaBuilder.equal(subQuestionRoot.get("trivio"), root),
                        criteriaBuilder.isMember(tag, subQuestionRoot.get("tags"))
                ));
                tagPredicates[i] = criteriaBuilder.greaterThan(subquery, 0L);
            }
            // Combine all tag predicates with AND operator
            return criteriaBuilder.and(tagPredicates);
        };
    }

}



