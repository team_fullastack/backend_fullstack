package ntnu.idatt2105.group44.trivioServer.service;

import java.util.ArrayList;
import java.util.List;

import ntnu.idatt2105.group44.trivioServer.dto.QuestionWithAnswers;
import ntnu.idatt2105.group44.trivioServer.model.Answer;
import ntnu.idatt2105.group44.trivioServer.model.Question;
import ntnu.idatt2105.group44.trivioServer.repository.AnswerRepository;
import ntnu.idatt2105.group44.trivioServer.repository.QuestionRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class QuestionService {

  private final QuestionRepository questionRepository;
  private final AnswerRepository answerRepository;

  private final AnswerService answerService;

  public QuestionService(QuestionRepository questionRepository, AnswerRepository answerRepository,
      AnswerService answerService) {
    this.questionRepository = questionRepository;
    this.answerService = answerService;
    this.answerRepository = answerRepository;
  }

  /**
   * Method to add a question to the question repository.
   *
   * @param question the question to add to the repository.
   */
  public void addQuestion(Question question) {
    questionRepository.save(question);
  }

  /**
   * Collective method to add a question with answers to the question repository, and answers to the
   * answer repository.
   *
   * @param question the question to be added.
   * @param answers  the answers of the question to be added.
   */
  @Transactional
  public void addQuestionWithAnswers(Question question, List<Answer> answers) {
    Question savedQuestion = questionRepository.save(question);
    for (Answer answer : answers) {
      answer.setQuestion(savedQuestion);
      answerRepository.save(answer);
    }
  }

  /**
   * Collective method to delete a question with its answers from both repositories. The method do
   * first delete all answers related to the question, and then the question. This should be
   * on-delete-cascade.
   *
   * @param question the question to be deleted (containing the answers).
   * @param answers  the answers to be deleted.
   */
  @Transactional
  public void deleteQuestionWithAnswers(Question question, List<Answer> answers) {
    answerRepository.deleteAll(answers);
    questionRepository.delete(question);
  }

  /**
   * Method to get all questions related to a trivio, with all their answers.
   *
   * @param trivioId the id of the trivio containing the questions with the answers.
   * @return the questions with answers in a list.
   */
  public List<QuestionWithAnswers> getQuestionsWithAnswersByTrivioId(Long trivioId) {
    List<Question> questions = questionRepository.findQuestionByTrivioId(trivioId);

    List<QuestionWithAnswers> questionsWithAnswersByTrivioId = new ArrayList<>();

    // Fetch answers for each question
    for (Question question : questions) {
      List<Answer> questionAnswers = answerRepository.findAnswersByQuestionId(
          question.getQuestionId());
      QuestionWithAnswers questionWithAnswers = new QuestionWithAnswers();
      questionWithAnswers.setQuestion(question);
      questionWithAnswers.setAnswers(questionAnswers);

      questionsWithAnswersByTrivioId.add(questionWithAnswers);
    }

    return questionsWithAnswersByTrivioId;
  }

  /**
   * Method to get all questions from the questions repository.
   *
   * @return all questions from the repository in a list.
   */
  public List<Question> getAllQuestions() {
    return questionRepository.findAll();
  }


  /**
   * Updates existing questions and their associated answers based on the provided lists of new and existing
   * questions with answers. Each pair of new and existing questions is processed, updating the question data
   * of the existing question and its associated answers.
   *
   * @param newQuestionsWithAnswers A list of {@link QuestionWithAnswers} objects representing the new questions
   *                                along with their associated answers.
   * @param existingQuestionsWithAnswers A list of {@link QuestionWithAnswers} objects representing the existing
   *                                     questions along with their associated answers.
   */
  @Transactional
  public void updateQuestionsAndAnswers(List<QuestionWithAnswers> newQuestionsWithAnswers,
      List<QuestionWithAnswers> existingQuestionsWithAnswers) {
    // Iterate over new and existing questions
    for (int i = 0;
        i < Math.min(newQuestionsWithAnswers.size(), existingQuestionsWithAnswers.size()); i++) {
      QuestionWithAnswers newQuestionWithAnswers = newQuestionsWithAnswers.get(i);
      QuestionWithAnswers oldQuestionWithAnswers = existingQuestionsWithAnswers.get(i);

      // Update question data
      Question oldQuestion = oldQuestionWithAnswers.getQuestion();
      Question newQuestion = newQuestionWithAnswers.getQuestion();
      oldQuestion.setQuestionType(newQuestion.getQuestionType());
      oldQuestion.setQuestion(newQuestion.getQuestion());
      oldQuestion.setTags(newQuestion.getTags());
      oldQuestion.setMedia(newQuestion.getMedia());

      // Update answers
      List<Answer> newAnswers = newQuestionWithAnswers.getAnswers();
      List<Answer> oldAnswers = oldQuestionWithAnswers.getAnswers();
      answerService.updateAnswers(newAnswers, oldAnswers, oldQuestion);

      questionRepository.save(oldQuestion);
    }

  }
}
