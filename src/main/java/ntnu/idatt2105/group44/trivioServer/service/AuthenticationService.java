package ntnu.idatt2105.group44.trivioServer.service;

import java.time.Duration;
import ntnu.idatt2105.group44.trivioServer.dto.LoginRequest;
import ntnu.idatt2105.group44.trivioServer.dto.SignUpRequest;
import ntnu.idatt2105.group44.trivioServer.model.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {
  private final UserService userService;
  private final PasswordEncoder passwordEncoder;


  public AuthenticationService(UserService userService, PasswordEncoder passwordEncoder) {
    this.userService = userService;
    this.passwordEncoder = passwordEncoder;
  }

  /**
   * Method to authenticate a user and its login-request. The method checks the credentials in the
   * login request and authenticate that the user-credentials are related to an exist the user repository.
   * The method returns false as long as one of the credentials do not exist or is related to the same user as
   * the other credentials.
   *
   * @param loginRequest the abstracted collection of user-credentials in a login-request (password and email).
   * @return true or false based on if a user exist with all the parameters in the login-request.
   */
  public boolean authenticateUser(LoginRequest loginRequest) {
    //if user doesn't exist return false
    if(!userService.checkIfUsernameIsPresent(loginRequest.getUsername())){
      return false;
    }
    //if user exists but the password is wrong, then throw error, then it will throw false.
    User user = userService.getUserByUsername(loginRequest.getUsername());
    String encodedPassword = passwordEncoder.encode(user.getPassword());
    return passwordEncoder.matches(loginRequest.getPassword(), encodedPassword);
  }

  /**
   * Method to check if credentials in a signup-request exist. The method checks they exist in the
   * user repository. The method return true as long as a one of the credentials exist.
   *
   * @param signUpRequest the abstracted collection of user-credentials in a signup-request (email and username)
   * @return true or false depending on if the credentials exist.
   */
  public boolean credentialsExists(SignUpRequest signUpRequest){
    if(userService.checkIfUsernameIsPresent(signUpRequest.getUsername())){
      return true;
    }
    return userService.checkIfEmailIsPresent(signUpRequest.getEmail());
  }
}
