package ntnu.idatt2105.group44.trivioServer.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Base64;
import java.util.Date;
import java.util.Map;
import java.util.logging.Logger;
import javax.crypto.SecretKey;
import ntnu.idatt2105.group44.trivioServer.controller.UserController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class JWTService {
  private static final Duration EXPIRATION_TIME = Duration.ofMinutes(10);
  private static final Logger logger = Logger.getLogger(JWTService.class.getName());
  @Value("${jwt.secret}")
  private String SECRET;


  /**
   * Method to get signing key by decoding the secret key with Base64 encoding.
   *
   * @return the signing key from the decoded secret key.
   */
  private SecretKey getSigningKey() {
    byte[] keyBytes = Base64.getDecoder().decode(this.SECRET.getBytes(StandardCharsets.UTF_8));
    return Keys.hmacShaKeyFor(keyBytes);
  }

  /**
   * Method to generate and build a token for a user. The method
   * includes user as the subject of the token, and authenticate the token using the signing key.
   *
   * @param id the user-id for the user of the token.
   * @return the generated token signed with the signing key.
   */
  public String generateToken(String id){
    return Jwts.builder()
        .subject(id)
        .issuer("IDATT2105GROUP5")
        .issuedAt(new Date(System.currentTimeMillis()))
        .expiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME.toMillis()))
        .signWith(getSigningKey())
        .compact();
  }

  /**
   * Method to parse token into claims.
   *
   * @param token the token to parse.
   * @return the claims from the parsed token.
   */
  public Jws<Claims> parseToken(String token) {
    return Jwts.parser()
        .verifyWith(getSigningKey())
        .build()
        .parseSignedClaims(token);
  }

  /**
   * Method to extract claims and get the payload from a token.
   *
   * @param token the token tro extract the claims from.
   * @return the claims, payload from the token.
   */
  public Claims extractClaims(String token) {
    return parseToken(token).getPayload();
  }

  /**
   * Method to extract the subject from claims, and identify the user from a token.
   *
   * @param token the token with the subject.
   * @return the user-id for the user as a string.
   */
  public String extractSubject(String token) {
    return extractClaims(token).getSubject();
  }

  /**
   * Method to validate an Authorization header bearing a token. This method
   * ensures that the format is correct and that the header is not empty.
   *
   * @param header the authentication header to validate.
   * @return true or false based on the validation of the header.
   */
  public boolean checkValidHeader(String header){
    return header != null && header.startsWith("Bearer ");
  }

  /**
   * Collective method to extract the subject directly from an Authorization header. This
   * method includes checking if the header is valid and extracting the subject, user from the
   * token in the header.
   *
   * @param header the header to extract the subject from.
   * @return the subject as a string. In this case the user-id as a string.
   */
  public String extractSubjectFromHeader(String header){
    if(!checkValidHeader(header)){
      throw new IllegalArgumentException("Header is not valid!");
    }
    // Extract the token from the "Bearer " prefix
    String jwtToken = header.substring(7);

    // Parse and extract the subject (user ID) from the JWT token
    return extractSubject(jwtToken);
  }
}
