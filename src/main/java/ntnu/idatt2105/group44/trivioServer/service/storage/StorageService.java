package ntnu.idatt2105.group44.trivioServer.service.storage;

import java.io.FileNotFoundException;
import ntnu.idatt2105.group44.trivioServer.exception.StorageException;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
@Service
public interface StorageService {

  /**
   * Saves the provided file to storage and returns the URL or path where it is stored.
   *
   * @param file the file to save
   * @return the URL or path of the saved file
   * @throws StorageException if an error occurs during file storage
   */
  String saveFile(MultipartFile file) throws StorageException;

  /**
   * Retrieves the file stored at the specified URL or path.
   *
   * @param fileUrl the URL or path of the file to retrieve
   * @return the file as a Resource
   * @throws FileNotFoundException if the file is not found
   */
  Resource getFile(String fileUrl) throws FileNotFoundException;

//  /**
//   * Deletes the file stored at the specified URL or path.
//   *
//   * @param fileUrl the URL or path of the file to delete
//   * @throws FileNotFoundException if the file is not found
//   * @throws StorageException       if an error occurs during file deletion
//   */
//  void deleteFile(String fileUrl) throws FileNotFoundException, StorageException;
}