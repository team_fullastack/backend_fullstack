package ntnu.idatt2105.group44.trivioServer.service;
import java.util.List;

import jakarta.persistence.EntityNotFoundException;
import java.util.logging.Logger;
import org.springframework.transaction.annotation.Transactional;
import ntnu.idatt2105.group44.trivioServer.dto.QuestionWithAnswers;
import ntnu.idatt2105.group44.trivioServer.dto.TrivioWithQAndA;
import ntnu.idatt2105.group44.trivioServer.exception.TrivioNotFoundException;
import ntnu.idatt2105.group44.trivioServer.model.Answer;
import ntnu.idatt2105.group44.trivioServer.model.Question;
import ntnu.idatt2105.group44.trivioServer.model.Trivio;
import ntnu.idatt2105.group44.trivioServer.model.User;
import ntnu.idatt2105.group44.trivioServer.repository.AnswerRepository;
import ntnu.idatt2105.group44.trivioServer.repository.QuestionRepository;
import ntnu.idatt2105.group44.trivioServer.repository.TrivioRepository;
import org.springframework.data.domain.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;

@Service
public class TrivioService {
  private final TrivioRepository trivioRepository;
  private final QuestionRepository questionRepository;
  private final AnswerRepository answerRepository;
  private final QuestionService questionService;
  private final UserService userService;
  private final JWTService jwtService;

  @Autowired
  public TrivioService(TrivioRepository trivioRepository, QuestionRepository questionRepository, QuestionService questionService,
                       AnswerRepository answerRepository, UserService userService, JWTService jwtService){
    this.trivioRepository = trivioRepository;
    this.questionRepository = questionRepository;
    this.answerRepository = answerRepository;
    this.questionService = questionService;
    this.userService = userService;
    this.jwtService = jwtService;
  }

  /**
   * Method to get all trivios. This method does not implement filtration.
   *
   * @return the list of all public trivios.
   */
  public List<Trivio> getAllTrivios(){
    return trivioRepository.findAll();
  }


  /**
   * Method to get all public trivios. This method does not implement filtration.
   *
   * @return the list of all the public trivios.
   */
  public List<Trivio> getAllPublicTrivios() {
    return trivioRepository.findTrivioByVisibility("public");
  }

  /**
   * Method to get all public trivios that is not linked to a user, meaning that they are
   * owned and created by other users. This method does not implement filtration.
   *
   * @param userId user-id for the user.
   * @return the list of public trivios that is not linked to the user.
   */
  public List<Trivio> getAllPublicTriviosFromOtherUsers(long userId) {
    return trivioRepository.findTrivioByVisibilityAndUserIdNot("public", userId);
  }

  /**
   * Method to get trivios by a user. This method does not implement filtration.
   *
   * @param userId the user-id of the user
   * @return the list of trivios belonging to this user.
   */
  public List<Trivio> getTriviosByUserId(Long userId){
    return trivioRepository.findTrivioByUserId(userId);
  }

  /**
   * Method to retrieve a trivio by its trivio-id.
   *
   * @param id the id of the trivio to retrieve.
   * @return returning the trivio if present.
   */
  public Trivio getTrivioById(Long id){
      return trivioRepository.findById(id).orElseThrow(() -> new TrivioNotFoundException(id));
  }

  /**
   * Method to retrieve a trivio by its title.
   * @param title the title of the trivio.
   * @return returning the trivio if  present.
   */
  public Trivio getTrivioByTitle(String title){
    return trivioRepository.findTrivioByTitle(title).orElseThrow(()
            -> new RuntimeException("Title do not exist in the repository"));
  }

  /**
   * Method to check if a user is the owner of a trivio.
   *
   * @param token the token that belong to the user.
   * @param id the trivio-id of the trivio.
   * @return true-false statement weather the user is the owner or not.
   */
  public boolean checkIfUserIsOwner(String token, Long id){
    try {
      Trivio trivio = getTrivioById(id);
      long userId = Long.parseLong(jwtService.extractSubjectFromHeader(token));
        return userId == trivio.getUser().getId();
    } catch (Exception e){
      throw new RuntimeException(e.getMessage());
    }
  }


  /**
   * Method to create a trivio with a user.
   *
   * @param trivioWithQAndA trivio with questions and answers.
   * @param userId the user-id of the creator.
   */
  @Transactional
  public void createTrivio(TrivioWithQAndA trivioWithQAndA, Long userId){
    try {
      Trivio trivio = trivioWithQAndA.getTrivio();
      User user = userService.getUserById(userId);

      trivio.setUser(user);

      List<QuestionWithAnswers> questionWithAnswers = trivioWithQAndA.getQuestionsWithAnswers();
      Trivio flushTrivio = trivioRepository.save(trivio);

      for (QuestionWithAnswers questionWithanswers : questionWithAnswers){
        Question question = questionWithanswers.getQuestion();
        List<Answer> answers = questionWithanswers.getAnswers();

        question.setTrivio(flushTrivio);

        questionService.addQuestionWithAnswers(question, answers);
      }
    } catch (Exception e){
      System.out.println(e.toString());
    }
  }

  /**
   * Method to edit a trivio. The method should ideally be more split up into multiple services.
   * Se inline comments for more description of functionally.
   *
   * @param editedTrivioWithQAndA the trivio with questions and answers that contain the updated information.
   * @param trivioId the trivio-id of the trivio to edit.
   */
//  @Transactional
//  public void editTrivio(TrivioWithQAndA editedTrivioWithQAndA, long trivioId) {
//    try {
//      Trivio trivio = trivioRepository.findById(trivioId).orElseThrow(() -> new TrivioNotFoundException(trivioId));
//      Trivio newTrivioInfo = editedTrivioWithQAndA.getTrivio();
//
//      //editing all the basic trivio information;
//      try {
//        trivio.setTitle(newTrivioInfo.getTitle());
//        trivio.setDescription(newTrivioInfo.getDescription());
//        trivio.setCategory(newTrivioInfo.getCategory());
//        trivio.setDifficulty(newTrivioInfo.getDifficulty());
//        trivio.setVisibility(newTrivioInfo.getVisibility());
//        trivio.setMultimedia_url(newTrivioInfo.getMultimedia_url());
//      } catch(Exception e){
//        throw new RuntimeException("Something went wrong when updating the trivio information: " + e.getMessage());
//      }
//
//      List<QuestionWithAnswers> newQuestionsWithAnswers = editedTrivioWithQAndA.getQuestionsWithAnswers();
//      List<QuestionWithAnswers> existingQuestionsWithAnswers = questionService.getQuestionsWithAnswersByTrivioId(trivioId);
//
//      //calculating the number of overlapping questions.
//      int questionMinSize = Math.min(newQuestionsWithAnswers.size(), existingQuestionsWithAnswers.size());
//
//      //updating all overlapping questions in the new and old list of questions.
//      for(int i=0; i < questionMinSize; i++) {
//        QuestionWithAnswers newQuestionWithAnswers = newQuestionsWithAnswers.get(i);
//        QuestionWithAnswers oldQuestionWithAnswers = existingQuestionsWithAnswers.get(i);
//
//        oldQuestionWithAnswers.getQuestion().setQuestionType(newQuestionWithAnswers.getQuestion().questionType);
//        oldQuestionWithAnswers.getQuestion().setQuestion(newQuestionWithAnswers.getQuestion().question);
//        oldQuestionWithAnswers.getQuestion().setTags(newQuestionWithAnswers.getQuestion().tags);
//        oldQuestionWithAnswers.getQuestion().setMedia(newQuestionWithAnswers.getQuestion().media);
//
//        List<Answer> newAnswers = newQuestionWithAnswers.getAnswers();
//        List<Answer> oldAnswers = oldQuestionWithAnswers.getAnswers();
//
//        //calculating the number of overlapping answers
//        int answerMinSize = Math.min(oldAnswers.size(), newAnswers.size());
//
//        //updating all overlapping questions in the new and old list of answers.
//        for(int j=0; j < answerMinSize; j++){
//          Answer newAnswer = newAnswers.get(j);
//          Answer oldAnswer = oldAnswers.get(j);
//
//          oldAnswer.setAnswer(newAnswer.getAnswer());
//          oldAnswer.setCorrect(newAnswer.isCorrect());
//
//          answerRepository.save(oldAnswer);
//        }
//
//        //if the question is changed from true-false to multiple, then more answers need to be added.
//        if(newAnswers.size() > oldAnswers.size()){
//          for(int j = answerMinSize; j < newAnswers.size(); j++){
//            Answer newAnswer = newAnswers.get(j);
//            newAnswer.setQuestion(oldQuestionWithAnswers.getQuestion());
//            answerRepository.save(newAnswer);
//          }
//        }
//
//        //if the question is changed from multiple to true-false, then some answers need to be removed.
//        if(newAnswers.size() < oldAnswers.size()){
//          for(int j = answerMinSize; j < oldAnswers.size(); j++){
//            answerRepository.delete(oldAnswers.get(j));
//          }
//        }
//        questionRepository.save(oldQuestionWithAnswers.getQuestion());
//      }
//
//      //if the new question-list is longer than the old question-list, more questions are added to the trivio
//      if(newQuestionsWithAnswers.size() > existingQuestionsWithAnswers.size()){
//        for(int i = questionMinSize; i < newQuestionsWithAnswers.size(); i++){
//          Question newQuestion = newQuestionsWithAnswers.get(i).getQuestion();
//          List<Answer> newAnswers = newQuestionsWithAnswers.get(i).getAnswers();
//          newQuestion.setTrivio(trivio);
//          questionService.addQuestionWithAnswers(newQuestion, newAnswers);
//        }
//      }
//
//      //if the new question-list is shorter than the old question-list, some questions are removed from the trivio.
//      if(newQuestionsWithAnswers.size() < existingQuestionsWithAnswers.size()){
//        for(int i = questionMinSize; i < existingQuestionsWithAnswers.size(); i++){
//          Question existingQuestion = existingQuestionsWithAnswers.get(i).getQuestion();
//          List<Answer> existingAnswers = existingQuestionsWithAnswers.get(i).getAnswers();
//          questionService.deleteQuestionWithAnswers(existingQuestion, existingAnswers);
//        }
//      }
//
//      trivioRepository.save(trivio); // Save the updated trivio
//    } catch (Exception e) {
//      throw new RuntimeException(e.getMessage());
//    }
//  }

  /**
   * Method to delete a trivio from the trivio-repository.
   *
   * @param id the id of the trivio to delete.
   */
  public void deleteTrivio(Long id){
    try {
      Trivio trivio = getTrivioById(id);
      trivioRepository.delete(trivio);
    } catch (Exception e){
      throw new RuntimeException(e.getMessage());
    }
  }

  public List<User> getUsersThatCanEdit(Long trivioId) {
    Trivio trivio = trivioRepository.findById(trivioId)
            .orElseThrow(() -> new EntityNotFoundException("Trivio not found with id: " + trivioId));
    return trivio.getUsersThatCanEdit();
  }

  /**
   * Method to add a user to a trivios editor-list. This means that the user is able to
   * edit the trivio, even though it do not belong to the user.
   *
   * @param trivioId the trivio-id of the trivio.
   * @param username the username of the user to add to the trivios editor-list.
   */
  public void addUserToTrivio(Long trivioId, String username) {
    try {
      Trivio trivio = getTrivioById(trivioId);
      User user = userService.getUserByUsername(username);
      trivio.addUserThatCanEdit(user);
      trivioRepository.save(trivio); // This will update the trivio and the join table
    } catch (Exception e){
      throw new RuntimeException(e.getMessage());
    }
  }

  /**
   * Method to remove a remove user from a trivios editor-list. This means that the user
   * is no longer able to edit the previously shared trivio.
   *
   * @param trivioId the trivio-id of the trivio.
   * @param username the username of the user to remove.
   */
  public void removeUserFromTrivioEditorList(Long trivioId, String username) {
    try {
      Trivio trivio = getTrivioById(trivioId);
      User user = userService.getUserByUsername(username);
      trivio.removeUserThatCanEdit(user);
      trivioRepository.save(trivio); // This will update the trivio and the join table
    } catch (Exception e){
      throw new RuntimeException(e.getMessage());
    }
  }

  /**
   * Method to check if a trivio is shared with a user. The method checks if the user exists
   * in the list of users that can edit the trivio.
   *
   * @param trivio the trivio that is or is not shared with the user.
   * @param userId the user-id of the user that can or cannot edit the trivio.
   * @return true if the trivio is shared with the user, or false if not.
   */
  public boolean isUserInListCanEdit(Trivio trivio, long userId) {
    List<User> usersThatCanEdit = trivio.getUsersThatCanEdit();
    if (usersThatCanEdit != null) {
      for (User user : usersThatCanEdit) {
        if (user.getId() == userId) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Method to retrieve public trivios that do not belong to a user. If any filter alternatives are added,
   * then the method will retrieve the filtrated result from the repository. The method includes pagination. Filter methods
   * are specified in the TrivioSpecification class.
   *
   * @param userId the user-id of the user that the public trivios do not belong to.
   * @param category the category for filtration.
   * @param difficulty the difficulty for filtration.
   * @param tags the tags for filtration.
   * @param pageable the page and max-number of trivios to retrieve.
   * @return the method returns the chosen page with public trivios that do not belong to the user.
   */
  public Page<Trivio> getFilteredPublicTriviosByOtherUsers(
          Long userId, String category, String difficulty,
          List<String> tags, String visibility, Pageable pageable) {

    Specification<Trivio> spec = TrivioSpecifications.filterByNotUserId(userId);

    spec = spec.and(TrivioSpecifications.filterByVisibility(visibility));

    //If a category is chosen
    if (category != null) {
      spec = spec.and(TrivioSpecifications.filterByCategory(category));
    }

    //If a difficulty is chosen
    if (difficulty != null) {
      spec = spec.and(TrivioSpecifications.filterByDifficulty(difficulty));
    }

    //If a tags are chosen
    if (tags != null && !tags.isEmpty()) {
      spec = spec.and(TrivioSpecifications.filterByTags(tags));
    }

    return trivioRepository.findAll(spec, pageable);
  }

  /**
   * Method to retrieve trivios that belong to a user from the trivio repository. If any filter alternatives are added,
   * then the method will retrieve the filtrated result from the repository. The method includes pagination. Filter methods
   * are specified in the TrivioSpecification class.
   *
   * @param userId the user-id of the user that the trivios belong to.
   * @param category the category for filtration.
   * @param difficulty the difficulty for filtration.
   * @param tags the tags for filtration.
   * @param pageable the page and max-number of trivios to retrieve.
   * @return the method returns the chosen page with trivios that belong to the user.
   */
  public Page<Trivio> getFilteredTriviosByUser(Long userId, String category, String difficulty, List<String> tags, Pageable pageable) {
    Specification<Trivio> spec = TrivioSpecifications.filterByUserId(userId);

    //If a category is chosen
    if (category != null) {
      spec = spec.and(TrivioSpecifications.filterByCategory(category));
    }

    //If a difficulty is chosen
    if (difficulty != null) {
      spec = spec.and(TrivioSpecifications.filterByDifficulty(difficulty));
    }

    //If a tags are chosen.
    if (tags != null && !tags.isEmpty()) {
      spec = spec.and(TrivioSpecifications.filterByTags(tags));
    }

    return trivioRepository.findAll(spec, pageable);
  }

  /**
   * Method to retrieve trivios that is shared with a user from the trivio repository. If any filter alternatives are added,
   * then the method will retrieve the filtrated result from the repository. The method includes pagination. Filter methods are
   * specified in the TrivioSpecification class.
   *
   * @param userId the user-id of the user that the trivios are shared with.
   * @param category the category for filtration.
   * @param difficulty the difficulty for filtration.
   * @param tags the tags for filtration.
   * @param pageable the page and max-number of trivios to retrieve.
   * @return the method returns the chosen page with trivios that are shared with the user.
   */
  public Page<Trivio> getFilteredSharedTriviosByUser(Long userId, String category, String difficulty, List<String> tags, Pageable pageable) {
    Specification<Trivio> spec = TrivioSpecifications.filterByUserInSharedList(userId);

    //If category is chosen, add filter method from trivio-specifications
    if (category != null) {
      spec = spec.and(TrivioSpecifications.filterByCategory(category));
    }

    //If difficulty is chosen, add filter method from trivio-specifications
    if (difficulty != null) {
      spec = spec.and(TrivioSpecifications.filterByDifficulty(difficulty));
    }

    //If tags are chosen, add filter method from trivio-specifications
    if (tags != null && !tags.isEmpty()) {
      spec = spec.and(TrivioSpecifications.filterByTags(tags));
    }

    return trivioRepository.findAll(spec, pageable);
  }


  @Transactional
  public void editTrivio(TrivioWithQAndA editedTrivioWithQAndA, long trivioId) {
    try {
      Trivio trivio = trivioRepository.findById(trivioId)
          .orElseThrow(() -> new TrivioNotFoundException(trivioId));
      Trivio newTrivioInfo = editedTrivioWithQAndA.getTrivio();

      // Update basic trivio information
      trivio.setTitle(newTrivioInfo.getTitle());
      trivio.setDescription(newTrivioInfo.getDescription());
      trivio.setCategory(newTrivioInfo.getCategory());
      trivio.setDifficulty(newTrivioInfo.getDifficulty());
      trivio.setVisibility(newTrivioInfo.getVisibility());
      trivio.setMultimedia_url(newTrivioInfo.getMultimedia_url());

      // Update questions and answers
      List<QuestionWithAnswers> newQuestionsWithAnswers = editedTrivioWithQAndA.getQuestionsWithAnswers();
      List<QuestionWithAnswers> existingQuestionsWithAnswers = questionService.getQuestionsWithAnswersByTrivioId(trivioId);
      questionService.updateQuestionsAndAnswers(newQuestionsWithAnswers, existingQuestionsWithAnswers);

      trivioRepository.save(trivio); // Save the updated trivio
    } catch (TrivioNotFoundException e) {
      throw e; // Rethrow TrivioNotFoundException to propagate it upwards
    } catch (Exception e) {
      throw new RuntimeException("Failed to edit trivio: " + e.getMessage(), e);
    }
  }
}


