package ntnu.idatt2105.group44.trivioServer.service;

import java.util.Date;
import java.util.List;
import ntnu.idatt2105.group44.trivioServer.dto.ResultDTO;
import ntnu.idatt2105.group44.trivioServer.model.Result;
import ntnu.idatt2105.group44.trivioServer.model.Trivio;
import ntnu.idatt2105.group44.trivioServer.model.User;
import ntnu.idatt2105.group44.trivioServer.repository.ResultRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ResultService {
  private final ResultRepository resultRepository;

  private final UserService userService;

  private final TrivioService trivioService;
  public ResultService(ResultRepository resultRepository, UserService userService, TrivioService trivioService){
    this.resultRepository = resultRepository;
    this.userService = userService;
    this.trivioService = trivioService;
  }

  /**
   * Method to add a DTO-abstracted result to the result repository. The method converts the
   * abstraction to the result-entity and adds it to the repository.
   *
   * @param resultDTO the DTO-abstracted result to be added to the repository.
   */
  public void addResult(ResultDTO resultDTO){
    Result result = convertToEntity(resultDTO);
    result.setPostedDate(new Date());
    resultRepository.save(result);
  }


  /**
   * Method to get a result by its result-id.
   *
   * @param id the result-id for the result.
   * @return the result with the result-id.
   */
  public Result getResultById(Long id){
    if(resultRepository.findById(id).isPresent()) {
      return resultRepository.findById(id).get();
    }
    return null;
  }

  /**
   * Method to get results related to a user-id from the result repository.
   * This method includes pagination.
   *
   * @param id the user-id for the user.
   * @param pageable the page and max-number of results to retrieve.
   * @return the method returns the chosen page with results related to the user-id.
   */
  public Page<Result> getResultByUserId(Long id, Pageable pageable){
    return resultRepository.getResultsByUserId(id, pageable);
  }

  /**
   * Method to get results related to a user-id and trivio-title from the result repository.
   * This method is used for result filtration and includes pagination.
   *
   * @param id the user-id for the user.
   * @param title the title for the trivio.
   * @param pageable the page and max-number of results to retrieve.
   * @return the method returns the chosen page with results related to the user-id and trivio-title.
   */
  public Page<Result> getResultByUserIdAndTitle(Long id, String title,Pageable pageable){
    return resultRepository.getResultsByUserIdAndTrivio_Title(id, title,pageable);
  }


  /**
   * Method to convert a result to a DTO-abstracted result containing the user, trivio and score relation.
   *
   * @param result the result to convert.
   * @return the DTO-abstracted result.
   */
  public ResultDTO convertToDTO(Result result){
    Long trivioId = result.getTrivio().getId();
    String username = result.getUser().getUsername();
    int score = result.getScore();
    return new ResultDTO(username,score,trivioId);
  }

  /**
   * Method to covert a DTO-abstracted result to a result entity with the user, trivio and score.
   *
   * @param resultDTO the DTO-abstracted result.
   * @return the result entity of the abstraction.
   */
  public Result convertToEntity(ResultDTO resultDTO){
    User user = userService.getUserByUsername(resultDTO.getUsername());
    Trivio trivio = trivioService.getTrivioById(resultDTO.getTrivioId());
    int score = resultDTO.getScore();
    Result result = new Result();
    result.setScore(score);
    result.setUser(user);
    result.setTrivio(trivio);
    return result;
  }

  /**
   * Method to get distinct trivio titles for a user from the result repository.
   *
   * @param userId the user-id for the user.
   * @return the results as a list of string, distinct trivio titles in the repository.
   */
  public List<String> getDistinctTrivioTitles(Long userId) {
    return resultRepository.findDistinctTrivioTitlesByUserId(userId);
  }

  /**
   * Method to get all results from the result repository.
   *
   * @return the results as a list of strings, all results from the repository.
   */
  public List<Result> getAllResults(){
    return resultRepository.findAll();
  }

}
