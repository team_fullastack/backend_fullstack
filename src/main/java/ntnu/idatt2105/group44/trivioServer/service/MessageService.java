package ntnu.idatt2105.group44.trivioServer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ntnu.idatt2105.group44.trivioServer.model.Message;
import ntnu.idatt2105.group44.trivioServer.repository.MessageRepository;
import java.util.List;

@Service
public class MessageService {

    private final MessageRepository messageRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    /**
     * Method to create message and add it to the message repository,
     *
     * @param message the message to add to the repository.
     * @return the status from the save method.
     */
    public Message createMessage(Message message) {
        return messageRepository.save(message);
    }

    /**
     * Method to get all messages from the message repository.
     *
     * @return all messages as a list of messages from the repository.
     */
    public List<Message> getAllMessages() {
        return messageRepository.findAll();
    }

}
