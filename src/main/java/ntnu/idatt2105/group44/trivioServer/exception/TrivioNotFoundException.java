package ntnu.idatt2105.group44.trivioServer.exception;

public class TrivioNotFoundException extends RuntimeException {
  public TrivioNotFoundException(Long id){
    super("Trivio id: "+id+" does not exist!");
  }
}
