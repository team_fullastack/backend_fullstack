package ntnu.idatt2105.group44.trivioServer.exception;
public class UserNotFoundException extends RuntimeException {
  public UserNotFoundException(Long id){
    super("User id:"+id+" couldn't be found");
  }

  public UserNotFoundException(String name){
    super("Username:"+name+" couldn't be found");
  }
}
