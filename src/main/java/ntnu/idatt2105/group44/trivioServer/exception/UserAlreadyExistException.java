package ntnu.idatt2105.group44.trivioServer.exception;

public class UserAlreadyExistException extends RuntimeException {
  private final String message;

  public UserAlreadyExistException(String message) {
    super(message);
    this.message = message;
  }

  public String getMessage() {
    return message;
  }
}
