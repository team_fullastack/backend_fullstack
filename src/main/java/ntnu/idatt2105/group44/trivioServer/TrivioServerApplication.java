package ntnu.idatt2105.group44.trivioServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrivioServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrivioServerApplication.class, args);
	}

}
