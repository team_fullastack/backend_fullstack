package ntnu.idatt2105.group44.trivioServer.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Base64;
import java.util.Collections;
import lombok.RequiredArgsConstructor;
import ntnu.idatt2105.group44.trivioServer.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
//extends OncePerRequest to ensure that filter is executed once per request

@Component
@RequiredArgsConstructor
public class JWTAuthorizationFilter extends OncePerRequestFilter {

  private static final Logger LOGGER = LogManager.getLogger(JWTAuthorizationFilter.class);


  @Value("${jwt.secret}")
  private String SECRET;


  private final UserDetailsService userDetailsService;

  //This method intercepts HTTP requests
  @Override
  protected void doFilterInternal(
      HttpServletRequest request,
      HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {

    // check Bearer auth header
    final String header = request.getHeader(HttpHeaders.AUTHORIZATION);
    if (header == null || !header.startsWith("Bearer ")) {
      filterChain.doFilter(request, response);
      return;
    }

    // if Bearer auth header exists, validate token, and extract userId from token.
    // Note also that the token comes in this format 'Bearer token'
    String token = header.substring(7);

    final String user_id = validateTokenAndGetUserId(token);
    if (user_id == null || SecurityContextHolder.getContext().getAuthentication() != null) {
      // validation failed or token expired
      filterChain.doFilter(request, response);
      return;
    }


    UserDetails userDetails = this.userDetailsService.loadUserByUsername(user_id);
    // if token is valid, add user details to the authentication context
    // Note that user details should be fetched from the database in real scenarios
    // this is case we will retrieve use details from mock
    UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
        userDetails,
        null,
        userDetails.getAuthorities()
    );
    auth.setDetails(
        new WebAuthenticationDetailsSource().buildDetails(request)
    );
    SecurityContextHolder.getContext().setAuthentication(auth);
    // then, continue with authenticated user context
    filterChain.doFilter(request, response);
  }

  public String validateTokenAndGetUserId(final String token) {
    try {
      final Algorithm hmac384 = Algorithm.HMAC384(Base64.getDecoder().decode(SECRET));

      final JWTVerifier verifier = JWT.require(hmac384).build();

      DecodedJWT decodedJWT = verifier.verify(token);

      return verifier.verify(token).getSubject();
    } catch (final JWTVerificationException verificationEx) {
      LOGGER.warn("token is invalid: {}", verificationEx.getMessage());
      return null;
    }
  }
}
