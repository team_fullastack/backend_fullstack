package ntnu.idatt2105.group44.trivioServer.config;

import java.util.Collection;
import java.util.Collections;
import lombok.RequiredArgsConstructor;
import ntnu.idatt2105.group44.trivioServer.exception.UserNotFoundException;
import ntnu.idatt2105.group44.trivioServer.model.Role;
import ntnu.idatt2105.group44.trivioServer.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@RequiredArgsConstructor
@Configuration
public class AppConfig {

  private final UserRepository userRepository;

  @Bean
  public UserDetailsService userDetailsService() {
    return id -> {
      // Load user details from the database using UserRepository
      ntnu.idatt2105.group44.trivioServer.model.User user = userRepository.findById(Long.valueOf(id)).orElseThrow(() -> new UserNotFoundException(id));
      if (user == null) {
        throw new UsernameNotFoundException("User not found");
      }

      // Create UserDetails object from loaded user details
      return User.builder()
          .username(user.getUsername())
          .password(user.getPassword())
          .authorities(mapRolesToAuthorities(user.getRole()))
          .build();
    };
  }
  // helper method to map roles to authorities
  // Makes it easier to implement roles to users
  private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Role role) {
    return Collections.singletonList(new SimpleGrantedAuthority("ROLE_" + role));
  }

}
