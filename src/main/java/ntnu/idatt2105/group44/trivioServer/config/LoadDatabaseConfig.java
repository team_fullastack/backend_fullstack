package ntnu.idatt2105.group44.trivioServer.config;
import java.util.List;

import ntnu.idatt2105.group44.trivioServer.model.*;
import ntnu.idatt2105.group44.trivioServer.repository.AnswerRepository;
import ntnu.idatt2105.group44.trivioServer.repository.QuestionRepository;
import ntnu.idatt2105.group44.trivioServer.repository.TrivioRepository;
import ntnu.idatt2105.group44.trivioServer.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDatabaseConfig {

  private static final Logger log = LoggerFactory.getLogger(LoadDatabaseConfig.class);
  @Bean
  CommandLineRunner initDataBase(TrivioRepository trivioRepository, UserRepository userRepository, QuestionRepository questionRepository, AnswerRepository answerRepository) {
    return args -> {
      User user1 = new User("test", "password", "username@mail.com", Role.USER);
      userRepository.save(user1);
      log.info("Preloading user: " + user1);

      User user2 = userRepository.save(new User("test2", "password", "test@mail.com",Role.USER));
      log.info("Preloading user: " + user2);

      // Create Trivio 1
      Trivio trivio1 = createTrivio("Capital Cities in Europe", "history","easy", "This is a trivio where you can test yourself on capitals in Europe",
          "public", "cat.png", user2,trivioRepository);
      Question question1_1 = createQuestion("What is the capital of France", "multiple",trivio1,List.of("France", "Geography", "Paris"),"history.png",questionRepository);
      Answer answer1_1_1 = createAnswer("Berlin", false, question1_1, answerRepository);
      Answer answer1_1_2 = createAnswer("Oslo", false, question1_1, answerRepository);
      Answer answer1_1_3 = createAnswer("Copenhagen", false, question1_1, answerRepository);
      Answer answer1_1_4 = createAnswer("Paris", true, question1_1, answerRepository);

      Question question1_2 = createQuestion("What is the capital of Germany","multiple", trivio1,List.of("Germany", "Capital"),"cat.png",questionRepository);
      Answer answer1_2_1 = createAnswer("Berlin", true, question1_2, answerRepository);
      Answer answer1_2_2 = createAnswer("Dusseldorf", false, question1_2, answerRepository);
      Answer answer1_2_3 = createAnswer("Copenhagen", false, question1_2, answerRepository);
      Answer answer1_2_4 = createAnswer("Hamburg", false, question1_2, answerRepository);

      Question question1_3 = createQuestion("What is the capital of The Netherlands", "multiple",trivio1,List.of("Netherlands", "Capital", "City"),"pngegg.png",questionRepository);
      Answer answer1_3_1 = createAnswer("London", false, question1_3, answerRepository);
      Answer answer1_3_2 = createAnswer("Brussels", false, question1_3, answerRepository);
      Answer answer1_3_3 = createAnswer("Amsterdam", true, question1_3, answerRepository);
      Answer answer1_3_4 = createAnswer("Warsaw", false, question1_3, answerRepository);

      trivio1.setQuestionList(List.of(question1_1));
      log.info("Preloading Trivio 1: " + trivio1);

      // Create Trivio 2
      Trivio trivio2 = createTrivio("World History", "random","medium", "Test your knowledge about world history",
          "public", "history.png", user1,trivioRepository);
      Question question2_1 = createQuestion("When was the World War II ended?","multiple", trivio2, List.of("War", "History", "Year"),"haunted-house.png",questionRepository);
      Answer answer2_1_4 = createAnswer("1945", true, question2_1,answerRepository);
      Answer answer2_1_3 = createAnswer("1939", false, question2_1,answerRepository);
      Answer answer2_1_2 = createAnswer("1916", false, question2_1,answerRepository);
      Answer answer2_1_1 = createAnswer("1923", false, question2_1,answerRepository);


      Question question2_2 = createQuestion("Who was the first man on the moon?","multiple", trivio2, List.of("Moon", "Space", "Person"),"something.png",questionRepository);
      Answer answer2_2_1 = createAnswer("Neil Armstrong", true, question2_2, answerRepository);
      Answer answer2_2_2 = createAnswer("Buzz Aldrin", false, question2_2, answerRepository);
      Answer answer2_2_3 = createAnswer("Buzz Lightyear", false, question2_2, answerRepository);
      Answer answer2_2_4 = createAnswer("Louis Armstrong", false, question2_2, answerRepository);


      Question question2_3 = createQuestion("Which countries were part of the allied in World War II?", "multiple",trivio2,List.of("War", "History", "Allies"), "flag_icon.png",questionRepository);
      Answer answer2_3_1 = createAnswer("The Great Britain", true, question2_3, answerRepository);
      Answer answer2_3_2 = createAnswer("France", true, question2_3, answerRepository);
      Answer answer2_3_3 = createAnswer("Japan", false, question2_3, answerRepository);
      Answer answer2_3_4 = createAnswer("Germany", false, question2_3, answerRepository);

      trivioRepository.save(trivio2);
      log.info("Preloading Trivio 2: " + trivio2);
    };
  }
  private Trivio createTrivio(String title, String category, String difficulty, String description, String visibility, String multimediaUrl, User user, TrivioRepository trivioRepository) {
    Trivio trivio = new Trivio();
    trivio.setTitle(title);
    trivio.setCategory(category);
    trivio.setDifficulty(difficulty);
    trivio.setDescription(description);
    trivio.setVisibility(visibility);
    trivio.setMultimedia_url(multimediaUrl);
    trivio.setUser(user);
    trivioRepository.save(trivio);
    return trivio;
  }

  private Question createQuestion(String questionText, String questionType, Trivio trivio,List<String> tags,String media, QuestionRepository questionRepository) {
    Question question = new Question();
    question.setQuestion(questionText);
    question.setQuestionType(questionType);
    question.setTrivio(trivio);
    question.setTags(tags);
    question.setMedia(media);
    questionRepository.save(question);
    return question;
  }

  private Answer createAnswer(String answerText, boolean correct, Question question, AnswerRepository answerRepository) {
    Answer answer = new Answer();
    answer.setAnswer(answerText);
    answer.setCorrect(correct);
    answer.setQuestion(question);
    answerRepository.save(answer);
    return answer;
  }

}

