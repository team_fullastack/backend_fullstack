package ntnu.idatt2105.group44.trivioServer.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name="trivio")
public class Trivio {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  public String title;

  public String description;

  public String category;

  public String difficulty;

  public String visibility; // kanskje vi burde ha en boolean istedenfor

  public String multimedia_url;

  @ManyToMany
  @JoinTable(
          name="trivio_editor",
          joinColumns = @JoinColumn(name="trivio_id"),
          inverseJoinColumns = @JoinColumn(name = "user_id")
  )
  private List<User> usersThatCanEdit;

  @ManyToOne
  @JoinColumn(name = "user_id")
  public User user;

  @JsonManagedReference
  @OneToMany(mappedBy = "trivio", cascade = CascadeType.ALL)
  public List<Question> questionList;


  public String getTitle() {return title;}

  public String getCategory(){return category;}

  public String getDescription() {
    return description;
  }

  public String getDifficulty(){return difficulty;}

  public String getVisibility(){return visibility;}

  public List<User> getUsersThatCanEdit(){return usersThatCanEdit;}

  public List<Question> getQuestionList(){
    return questionList;
  }

  public User getUser() {
    return user;
  }

  public Long getId() {
    return id;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setCategory(String category){this.category = category;}

  public void setDescription(String description) {
    this.description = description;
  }

  public void setDifficulty(String difficulty) {
    this.difficulty = difficulty;
  }

  public void setVisibility(String visibility) {
    this.visibility = visibility;
  }

  public void setMultimedia_url(String multimedia_url) {
    this.multimedia_url = multimedia_url;
  }

  public void setQuestionList(
      List<Question> questionList) {
    this.questionList = questionList;
  }

  public void setUsersThatCanEdit(List<User> usersThatCanEdit){
    this.usersThatCanEdit = usersThatCanEdit;
  }

  public void addUserThatCanEdit(User user) {
    if (user != null && !this.usersThatCanEdit.contains(user)) {
      this.usersThatCanEdit.add(user);
    }
  }

  public void removeUserThatCanEdit(User user) {
    this.usersThatCanEdit.remove(user);
  }

  public String getMultimedia_url() {
    return multimedia_url;
  }

  public void setId(long l) {
    this.id = l;
  }
}
