package ntnu.idatt2105.group44.trivioServer.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.util.Date;

@Entity
public class Result {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private int score;

  @ManyToOne
  @JoinColumn(name = "user_id")
  private User user;

  @ManyToOne
  @JoinColumn(name = "trivio_id")
  private Trivio trivio;

  @JsonFormat
      (shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
  @Temporal(TemporalType.TIMESTAMP) // Use this annotation for java.util.Date
  private Date postedDate;

  public Result(){}

  public Long getId() {
    return id;
  }

  public int getScore() {
    return score;
  }

  public User getUser() {
    return user;
  }

  public Trivio getTrivio() {
    return trivio;
  }

  public void setScore(int score) {
    this.score = score;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public void setTrivio(Trivio trivio) {
    this.trivio = trivio;
  }

  public Date getPostedDate() {
    return postedDate;
  }

  public void setPostedDate(Date postedDate) {
    this.postedDate = postedDate;
  }
}
