package ntnu.idatt2105.group44.trivioServer.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.CascadeType;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import java.util.List;

@Entity
public class Question {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  public String question;

  public String questionType;


  public String media;
  @ElementCollection
  public List<String> tags;

  @JsonManagedReference
  @OneToMany(mappedBy = "question", cascade = CascadeType.ALL)
  public List<Answer> answers;

  @JsonBackReference
  @ManyToOne
  @JoinColumn(name = "trivio_id")
  public Trivio trivio;


  public Question() {}

  public List<String> getTags(){return tags;}

  public String getQuestion() {
    return question;
  }

  public String getQuestionType(){return questionType;}

  public List<Answer> getAnswers() {
    return answers;
  }
  public Long getQuestionId(){return id;}

  public String getMedia(){
    return media;
  }

  public void setTrivio(Trivio trivio) {
    this.trivio = trivio;
  }

  public void setQuestionType(String questionType){this.questionType = questionType;}

  public void setTags(List<String> tags){this.tags = tags;}

  public void setAnswers(List<Answer> answers) {
    this.answers = answers;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public void setMedia(String media) {
    this.media = media;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
