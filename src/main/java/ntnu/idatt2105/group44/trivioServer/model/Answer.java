package ntnu.idatt2105.group44.trivioServer.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import ntnu.idatt2105.group44.trivioServer.repository.QuestionRepository;

@Entity
public class Answer {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  public String answer;

  boolean correct;

  public Answer() {

  }


  public Long getId() {
    return id;
  }

  public String getAnswer() {
    return answer;
  }

  @JsonBackReference
  @ManyToOne
  @JoinColumn(name = "question_id")
  public Question question;

  public boolean isCorrect() {
    return correct;
  }

  public void setQuestion(Question question){
    this.question = question;
  }

  public void setCorrect(boolean correct) {
    this.correct = correct;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
