package ntnu.idatt2105.group44.trivioServer.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import ntnu.idatt2105.group44.trivioServer.dto.LoginRequest;
import ntnu.idatt2105.group44.trivioServer.model.User;

@ExtendWith(MockitoExtension.class)
class AuthenticationServiceTest {
    
    @Mock
    private UserService userService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private AuthenticationService authenticationService;

    @Test
    void testAuthenticateUser() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("username");
        loginRequest.setPassword("password");

        User user = new User();
        user.setUsername("username");
        user.setPassword("password");

        when(userService.checkIfUsernameIsPresent(loginRequest.getUsername())).thenReturn(true);
        when(userService.getUserByUsername(loginRequest.getUsername())).thenReturn(user);
        when(passwordEncoder.encode(user.getPassword())).thenReturn(user.getPassword());
        when(passwordEncoder.matches(loginRequest.getPassword(), user.getPassword())).thenReturn(true);

        boolean result = authenticationService.authenticateUser(loginRequest);

        assertTrue(result);
    }

    @Test
    void testAuthenticateUser_UserDoesNotExist() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("username");
        loginRequest.setPassword("password");

        when(userService.checkIfUsernameIsPresent(loginRequest.getUsername())).thenReturn(false);

        boolean result = authenticationService.authenticateUser(loginRequest);

        assertTrue(!result);
    }

    @Test
    void testAuthenticateUser_WrongPassword() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("username");
        loginRequest.setPassword("WrongPassword");

        User user = new User();
        user.setUsername("username");
        user.setPassword("pass");

        when(userService.checkIfUsernameIsPresent(loginRequest.getUsername())).thenReturn(true);
        when(userService.getUserByUsername(loginRequest.getUsername())).thenReturn(user);
        when(passwordEncoder.encode(user.getPassword())).thenReturn(user.getPassword());
        when(passwordEncoder.matches(loginRequest.getPassword(), user.getPassword())).thenReturn(false);

        boolean result = authenticationService.authenticateUser(loginRequest);

        assertTrue(!result);
    }
}
