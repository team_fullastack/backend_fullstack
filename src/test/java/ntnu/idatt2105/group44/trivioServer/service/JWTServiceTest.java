package ntnu.idatt2105.group44.trivioServer.service;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import java.security.Key;
import java.util.Base64;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
class JWTServiceTest {

  @Mock
  private Key key;

  @InjectMocks
  private JWTService jwtService;

  @BeforeEach
  void setUp() {
    // Set a default value for SECRET
    ReflectionTestUtils.setField(jwtService, "SECRET", "testSecrettestSecrettestSecrettestSecrettestSecrettestSecret");
  }
  @Test
  void testGenerateToken() {
    String token = jwtService.generateToken("user123");

    assertNotNull(token);
  }

  @Test
  void testParseToken() {

    String token = jwtService.generateToken("user123");

    // parsing token
    Jws<Claims> claims = jwtService.parseToken(token);

    assertNotNull(claims);
    assertEquals("user123", claims.getPayload().getSubject());
  }

  @Test
  void testExtractSubjectFromHeader() {

    String token = jwtService.generateToken("user123");
    String header = "Bearer " + token;

    String subject = jwtService.extractSubjectFromHeader(header);

    assertEquals("user123", subject);
  }

  @Test
  void testExtractSubjectFromHeaderWithInvalidHeader() {
    assertThrows(IllegalArgumentException.class, () -> {
      jwtService.extractSubjectFromHeader("InvalidHeader");
    });
  }
}