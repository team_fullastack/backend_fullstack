package ntnu.idatt2105.group44.trivioServer.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.AdditionalMatchers.eq;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ntnu.idatt2105.group44.trivioServer.dto.QuestionWithAnswers;
import ntnu.idatt2105.group44.trivioServer.dto.TrivioWithQAndA;
import ntnu.idatt2105.group44.trivioServer.model.Answer;
import ntnu.idatt2105.group44.trivioServer.model.Question;
import ntnu.idatt2105.group44.trivioServer.model.Trivio;
import ntnu.idatt2105.group44.trivioServer.model.User;
import ntnu.idatt2105.group44.trivioServer.repository.TrivioRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

@ExtendWith(MockitoExtension.class)
public class TrivioServiceTest {

    @Mock
    private UserService userService;

    @Mock
    private TrivioRepository trivioRepository;

    @Mock
    private QuestionService questionService;

    @InjectMocks
    private TrivioService trivioService;

    @Test
    void testGetAllTrivios() {
        Trivio trivio1 = new Trivio();

        Trivio trivio2 = new Trivio();

        when(trivioRepository.findAll()).thenReturn(java.util.List.of(trivio1, trivio2));

        assertEquals(2, trivioService.getAllTrivios().size());
    }

    @Test
    void testGetAllPublicTrivios() {
        Trivio trivio1 = new Trivio();
        trivio1.setVisibility("public");

        Trivio trivio2 = new Trivio();
        trivio2.setVisibility("public");

        when(trivioRepository.findTrivioByVisibility("public")).thenReturn(
            java.util.List.of(trivio1, trivio2));

        assertEquals(2, trivioService.getAllPublicTrivios().size());
    }

    @Test
    void testGetAllPublicTriviosFromOtherUsers() {
        Trivio trivio1 = new Trivio();
        trivio1.setVisibility("public");

        Trivio trivio2 = new Trivio();
        trivio2.setVisibility("public");

        when(trivioRepository.findTrivioByVisibilityAndUserIdNot("public", 1L))
            .thenReturn(java.util.List.of(trivio1, trivio2));

        assertEquals(2, trivioService.getAllPublicTriviosFromOtherUsers(1L).size());
    }

    @Test
    void testGetTriviosByUserId() {
        Trivio trivio1 = new Trivio();

        Trivio trivio2 = new Trivio();

        Trivio trivio3 = new Trivio();

        when(trivioRepository.findTrivioByUserId(1L)).thenReturn(
            java.util.List.of(trivio1, trivio2, trivio3));

        assertEquals(3, trivioService.getTriviosByUserId(1L).size());
    }

    @Test
    void testGetTrivioById() {
        Trivio trivio = new Trivio();
        trivio.setTitle("Trivio 1");

        when(trivioRepository.findById(1L)).thenReturn(java.util.Optional.of(trivio));

        assertEquals("Trivio 1", trivioService.getTrivioById(1L).getTitle());
    }

    @Test
    void testGetTrivioByIdThrowsException() {
        when(trivioRepository.findById(1L)).thenReturn(java.util.Optional.empty());

        try {
            trivioService.getTrivioById(1L);
        } catch (Exception e) {
            assertEquals("Trivio id: 1 does not exist!", e.getMessage());
        }
    }

    @Test
    void testGetTrivioByTitle() {
        Trivio trivio = new Trivio();
        trivio.setTitle("Trivio 1");

        when(trivioRepository.findTrivioByTitle("Trivio 1")).thenReturn(
            java.util.Optional.of(trivio));

        assertEquals("Trivio 1", trivioService.getTrivioByTitle("Trivio 1").getTitle());
    }

    @Test
    void testGetTrivioByTitleThrowsRuntimeException() {
        when(trivioRepository.findTrivioByTitle("Trivio 1")).thenReturn(java.util.Optional.empty());

        assertThrows(RuntimeException.class, () -> {
            trivioService.getTrivioByTitle("Trivio 1");
        });
    }

    @Test
    void testCreateTrivio() {
        Trivio trivio = new Trivio();
        trivio.setTitle("Trivio 1");

        TrivioWithQAndA trivioWithQAndA = new TrivioWithQAndA();
        trivioWithQAndA.setTrivio(trivio);

        Answer answer = new Answer();
        answer.setAnswer("Answer 1");

        Answer answer2 = new Answer();
        answer2.setAnswer("Answer 2");

        Question question = new Question();
        question.setQuestion("Question 1");

        QuestionWithAnswers questionWithAnswers = new QuestionWithAnswers();
        questionWithAnswers.setQuestion(question);
        questionWithAnswers.setAnswers(java.util.List.of(answer, answer2));

        trivioWithQAndA.setQuestionsWithAnswers(java.util.List.of(questionWithAnswers));

        User user = new User();
        user.setId(1L);

        when(userService.getUserById(1L)).thenReturn(user);
        when(trivioRepository.save(any(Trivio.class))).thenReturn(trivio);

        trivioService.createTrivio(trivioWithQAndA, 1L);

        verify(trivioRepository, times(1)).save(any(Trivio.class));
        verify(questionService, times(1)).addQuestionWithAnswers(any(Question.class), anyList());
    }

    @Test
    void testEditTrivio() {
        Trivio originalTrivio = new Trivio();
        originalTrivio.setTitle("Original Trivio");

        when(trivioRepository.findById(1L)).thenReturn(java.util.Optional.of(originalTrivio));

        Trivio newTrivio = new Trivio();
        newTrivio.setTitle("New Trivio");

        TrivioWithQAndA editedTrivioWithQAndA = new TrivioWithQAndA();
        editedTrivioWithQAndA.setTrivio(newTrivio);
        editedTrivioWithQAndA.setQuestionsWithAnswers(new ArrayList<>());

        trivioService.editTrivio(editedTrivioWithQAndA, 1L);

        verify(trivioRepository, times(1)).findById(1L);
        assertEquals("New Trivio", originalTrivio.getTitle());
    }

    @Test
    void testDeleteTrivio() {
        // Mocking data
        Trivio trivio = new Trivio();
        long trivioId = 1L;

        // Mocking repository behavior
        when(trivioRepository.findById(trivioId)).thenReturn(Optional.of(trivio));

        // Method under test
        trivioService.deleteTrivio(trivioId);

        verify(trivioRepository, times(1)).delete(trivio);
    }

    @Test
    void testGetUsersThatCanEdit() {
        Trivio trivio = new Trivio();
        User user1 = new User();
        User user2 = new User();
        List<User> usersThatCanEdit = new ArrayList<>();
        usersThatCanEdit.add(user1);
        usersThatCanEdit.add(user2);
        trivio.setUsersThatCanEdit(usersThatCanEdit);
        long trivioId = 1L;
        when(trivioRepository.findById(trivioId)).thenReturn(Optional.of(trivio));

        List<User> users = trivioService.getUsersThatCanEdit(trivioId);

        assertEquals(2, users.size());
        assertTrue(users.contains(user1));
        assertTrue(users.contains(user2));
    }

    @Test
    public void testAddUserToTrivio() {
        // Mock data
        Long trivioId = 1L;
        String username = "testuser";
        Trivio trivio = mock(Trivio.class);
        User user = new User();
        when(trivioRepository.findById(trivioId)).thenReturn(Optional.of(trivio));
        when(userService.getUserByUsername(username)).thenReturn(user);

        trivioService.addUserToTrivio(trivioId, username);

        verify(trivio, times(1)).addUserThatCanEdit(user);

        verify(trivioRepository, times(1)).save(trivio);
    }

    @Test
    public void testRemoveUserFromTrivio() {
        // Mock data
        Long trivioId = 1L;
        String username = "testuser";
        Trivio trivio = mock(Trivio.class);
        User user = new User();
        when(trivioRepository.findById(trivioId)).thenReturn(Optional.of(trivio));
        when(userService.getUserByUsername(username)).thenReturn(user);

        trivioService.removeUserFromTrivioEditorList(trivioId, username);

        verify(trivio, times(1)).removeUserThatCanEdit(user);

        verify(trivioRepository, times(1)).save(trivio);
    }

    @Test
    public void testIsUserInListCanEdit() {
        // Mock data
        Trivio trivio = new Trivio(); // Assuming you have a trivio object
        User user = new User();
        user.setId(1L);
        List<User> usersThatCanEdit = new ArrayList<>();
        usersThatCanEdit.add(user);
        trivio.setUsersThatCanEdit(usersThatCanEdit);

        // Call the method
        boolean result = trivioService.isUserInListCanEdit(trivio, user.getId());

        assertTrue(result);

    }


    @Test
    void testGetFilteredPublicTriviosByOtherUsers() {
        // Mocking parameters
        Long userId = 1L;
        String category = "History";
        String difficulty = "Easy";
        List<String> tags = List.of("tag1", "tag2");
        String visibility = "public";
        Pageable pageable = Pageable.ofSize(10).withPage(0);

        // Mocking repository behavior
        Page<Trivio> expectedPage = mock(Page.class);
        when(trivioRepository.findAll(any(Specification.class), any(Pageable.class))).thenReturn(
            expectedPage);

        // Call the method
        Page<Trivio> result = trivioService.getFilteredPublicTriviosByOtherUsers(userId, category,
            difficulty, tags, visibility, pageable);

        // Verify the result
        assertEquals(expectedPage, result);
    }

    @Test
    void testGetFilteredTriviosByUser() {
        // Mocking parameters
        Long userId = 1L;
        String category = "History";
        String difficulty = "Easy";
        List<String> tags = List.of("tag1", "tag2");
        Pageable pageable = Pageable.ofSize(10).withPage(0);

        Page<Trivio> expectedPage = mock(Page.class);
        when(trivioRepository.findAll(any(Specification.class), any(Pageable.class))).thenReturn(
            expectedPage);

        Page<Trivio> result = trivioService.getFilteredTriviosByUser(userId, category, difficulty,
            tags, pageable);

        assertEquals(expectedPage, result);
    }
    @Test
    void testGetFilteredSharedTriviosByUser() {
        // Mocking parameters
        Long userId = 1L;
        String category = "History";
        String difficulty = "Easy";
        List<String> tags = List.of("tag1", "tag2");
        Pageable pageable = Pageable.ofSize(10).withPage(0);

        // Mocking repository behavior
        Page<Trivio> expectedPage = mock(Page.class);
        when(trivioRepository.findAll(any(Specification.class), any(Pageable.class))).thenReturn(expectedPage);

        // Call the method
        Page<Trivio> result = trivioService.getFilteredSharedTriviosByUser(
            userId, category, difficulty, tags, pageable);

        // Verify the result
        assertEquals(expectedPage, result);
    }
}
