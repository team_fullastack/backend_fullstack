package ntnu.idatt2105.group44.trivioServer.service;

import ntnu.idatt2105.group44.trivioServer.service.storage.LocalFileStorageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import java.io.*;
import java.nio.file.Path;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LocalStorageServiceTest {

  private LocalFileStorageService fileStorageService;

  @TempDir
  Path tempDir;

  @BeforeEach
  void setUp() {
    fileStorageService = new LocalFileStorageService();
    fileStorageService.setUploadDirectory(tempDir.toString());
  }

  @Test
  void testSaveFileAndRetrieveFile() throws IOException, FileNotFoundException {
    // Create a test file
    File testFile = createTestFile("test.txt", "Hello, World!");

    // Convert the test file to a MockMultipartFile
    MockMultipartFile multipartFile = new MockMultipartFile(
        "file", testFile.getName(), "text/plain", new FileInputStream(testFile));

    // Save the file
    String savedFileName = fileStorageService.saveFile(multipartFile);

    // Assert that the file is saved
    assertNotNull(savedFileName);

    // Retrieve the file
    Resource retrievedFile = fileStorageService.getFile(savedFileName);

    // Assert that the retrieved file exists and is readable
    assertTrue(retrievedFile.exists());
    assertTrue(retrievedFile.isReadable());

    // Assert the content of the retrieved file
    try (InputStream inputStream = retrievedFile.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
      assertEquals("Hello, World!", reader.readLine());
    }
  }

  private File createTestFile(String fileName, String content) throws IOException {
    File testFile = new File(tempDir.toFile(), fileName);
    try (PrintWriter writer = new PrintWriter(testFile)) {
      writer.print(content);
    }
    return testFile;
  }
}