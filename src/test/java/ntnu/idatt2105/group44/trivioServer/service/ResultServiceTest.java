package ntnu.idatt2105.group44.trivioServer.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import ntnu.idatt2105.group44.trivioServer.dto.ResultDTO;
import ntnu.idatt2105.group44.trivioServer.model.Result;
import ntnu.idatt2105.group44.trivioServer.model.Trivio;
import ntnu.idatt2105.group44.trivioServer.model.User;
import ntnu.idatt2105.group44.trivioServer.repository.ResultRepository;

@ExtendWith(MockitoExtension.class)
class ResultServiceTest {

    @Mock
    private ResultRepository resultRepository;

    @Mock 
    private UserService userService;

    @Mock
    private TrivioService trivioService;

    @InjectMocks
    private ResultService resultService;

    @Test
    void testAddResult() {
        ResultDTO resultDTO = new ResultDTO("TestUser", 5, 5L);
        
        resultService.addResult(resultDTO);
        
        verify(resultRepository).save(any(Result.class));
    }
    
    @Test
    void testGetResultById() {
        Result result = new Result();

        when(resultRepository.findById(5L)).thenReturn(java.util.Optional.of(result));

        Result foundResult = resultService.getResultById(5L);

        verify(resultRepository, times(2)).findById(5L);

        assertEquals(result, foundResult);
    }

    @Test
    void testGetResultByIdReturnNull() {
        when(resultRepository.findById(5L)).thenReturn(java.util.Optional.empty());

        Result foundResult = resultService.getResultById(5L);

        verify(resultRepository, times(1)).findById(5L);

        assertEquals(null, foundResult);
    }

    @Test
    void testGetResultByUserId() {
        Pageable pageable = Pageable.unpaged();

        List<Result> resultList = new ArrayList<>();
        resultList.add(new Result());
        resultList.add(new Result());

        when(resultRepository.getResultsByUserId(1L, pageable)).thenReturn(new PageImpl<>(resultList));

        Page<Result> resultPage = resultService.getResultByUserId(1L, pageable);

        assertEquals(resultList.size(), resultPage.getTotalElements());
    }

    @Test
    void testConvertToDTO() {
        Result result = new Result();
        User user = new User();
        user.setUsername("TestUser");
        Trivio trivio = new Trivio();
        result.setUser(user);
        result.setTrivio(trivio);
        result.setScore(5);

        ResultDTO resultDTO = resultService.convertToDTO(result);

        assertEquals("TestUser", resultDTO.getUsername());
        assertEquals(5, resultDTO.getScore());
    }

    @Test
    void testConvertToEntity() {
        ResultDTO resultDTO = new ResultDTO("TestUser", 5, 5L);
        User user = new User();
        user.setUsername("TestUser");
        Trivio trivio = new Trivio();

        when(userService.getUserByUsername("TestUser")).thenReturn(user);
        when(trivioService.getTrivioById(5L)).thenReturn(trivio);

        Result result = resultService.convertToEntity(resultDTO);

        assertEquals("TestUser", result.getUser().getUsername());
        assertEquals(5, result.getScore());
    }

    @Test
    void testGetAllResults() {
        Result result1 = new Result();
        Result result2 = new Result();

        when(resultRepository.findAll()).thenReturn(java.util.List.of(result1, result2));

        assertEquals(2, resultService.getAllResults().size());
    }
}
