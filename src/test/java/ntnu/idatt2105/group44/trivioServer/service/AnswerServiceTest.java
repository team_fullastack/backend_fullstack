package ntnu.idatt2105.group44.trivioServer.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import ntnu.idatt2105.group44.trivioServer.model.Answer;
import ntnu.idatt2105.group44.trivioServer.model.Question;
import ntnu.idatt2105.group44.trivioServer.repository.AnswerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AnswerServiceTest {
  @Mock
  private AnswerRepository answerRepository;

  @InjectMocks
  private AnswerService answerService;


  @Test
  void testUpdateAnswers() {
    // Given
    Question question = new Question();
    question.setId(1L);

    Answer oldAnswer1 = new Answer();
    oldAnswer1.setId(1L);
    oldAnswer1.setAnswer("Old Answer 1");
    oldAnswer1.setCorrect(true);

    Answer oldAnswer2 = new Answer();
    oldAnswer2.setId(2L);
    oldAnswer2.setAnswer("Old Answer 2");
    oldAnswer2.setCorrect(false);

    Answer newAnswer1 = new Answer();
    newAnswer1.setId(1L);
    newAnswer1.setAnswer("New Answer 1");
    newAnswer1.setCorrect(true);

    Answer newAnswer2 = new Answer();
    newAnswer2.setId(2L);
    newAnswer2.setAnswer("New Answer 2");
    newAnswer2.setCorrect(false);

    List<Answer> newAnswers = List.of(newAnswer1, newAnswer2);
    List<Answer> oldAnswers = List.of(oldAnswer1, oldAnswer2);

    // When
    answerService.updateAnswers(newAnswers, oldAnswers, question);
    verify(answerRepository).save(oldAnswer1);
    verify(answerRepository).save(oldAnswer2);
    verify(answerRepository, never()).delete(any(Answer.class)); // No deletion should occur
  }

  @Test
  void testUpdateAnswers_AddNewAnswers() {
    // Given
    Question question = new Question();
    question.setId(1L);

    Answer oldAnswer1 = new Answer();
    oldAnswer1.setId(1L);
    oldAnswer1.setAnswer("Old Answer 1");
    oldAnswer1.setCorrect(true);

    List<Answer> newAnswers = new ArrayList<>();
    Answer newAnswer1 = new Answer();
    newAnswer1.setId(2L);
    newAnswer1.setAnswer("New Answer 1");
    newAnswer1.setCorrect(false);
    newAnswers.add(newAnswer1);
    Answer newAnswer2 = new Answer();
    newAnswer2.setId(3L);
    newAnswer2.setAnswer("New Answer 2");
    newAnswer2.setCorrect(false);
    newAnswers.add(newAnswer2);
    List<Answer> oldAnswers = List.of(oldAnswer1);

    // Mock repository behavior
    when(answerRepository.save(any(Answer.class))).thenAnswer(invocation -> invocation.getArgument(0));
    // When
    answerService.updateAnswers(newAnswers, oldAnswers, question);

    // Then
    verify(answerRepository).save(newAnswer2);
    verify(answerRepository, never()).delete(any(Answer.class)); // No deletion should occur
  }
  @Test
  void testUpdateAnswers_DeleteExcessData() {
    // Given
    Question question = new Question();
    question.setId(1L);

    Answer oldAnswer1 = new Answer();
    oldAnswer1.setId(1L);
    oldAnswer1.setAnswer("Old Answer 1");
    oldAnswer1.setCorrect(true);
    Answer oldAnswer2 = new Answer();
    oldAnswer2.setId(3L);
    oldAnswer2.setAnswer("Old answer 2");
    oldAnswer2.setCorrect(false);
    List<Answer> newAnswers = new ArrayList<>();
    Answer newAnswer1 = new Answer();
    newAnswer1.setId(2L);
    newAnswer1.setAnswer("New Answer 1");
    newAnswer1.setCorrect(false);
    newAnswers.add(newAnswer1);

    List<Answer> oldAnswers = List.of(oldAnswer1,oldAnswer2);

    // Mock repository behavior
    when(answerRepository.save(any(Answer.class))).thenAnswer(invocation -> invocation.getArgument(0));
    // When
    answerService.updateAnswers(newAnswers, oldAnswers, question);

    verify(answerRepository).save(oldAnswer1);
    verify(answerRepository).delete(oldAnswer2);
  }
}
