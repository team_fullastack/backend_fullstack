package ntnu.idatt2105.group44.trivioServer.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import ntnu.idatt2105.group44.trivioServer.dto.QuestionWithAnswers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ntnu.idatt2105.group44.trivioServer.model.Answer;
import ntnu.idatt2105.group44.trivioServer.model.Question;
import ntnu.idatt2105.group44.trivioServer.repository.AnswerRepository;
import ntnu.idatt2105.group44.trivioServer.repository.QuestionRepository;

@ExtendWith(MockitoExtension.class)
class QuestionServiceTest {

    @Mock
    private QuestionRepository questionRepository;

    @Mock
    private AnswerRepository answerRepository;

    @Mock
    private AnswerService answerService;
    @InjectMocks
    private QuestionService questionService;




    @Test
    void testAddQuestion() {
        Question question = new Question();
        question.setQuestion("Test question");

        questionService.addQuestion(question);

        verify(questionRepository, times(1)).save(question);
    }

    @Test
    void testAddQuestionWithAnswers() {
        Answer answer1 = new Answer();
        answer1.setAnswer("Answer 1");

        Answer answer2 = new Answer();
        answer2.setAnswer("Answer 2");

        Question question = new Question();
        question.setQuestion("Test question");

        questionService.addQuestionWithAnswers(question, java.util.List.of(answer1, answer2));

        verify(questionRepository, times(1)).save(question);
        verify(answerRepository, times(1)).save(answer1);
        verify(answerRepository, times(1)).save(answer2);
    }

    @Test
    void testDeleteQuestionWithAnswers() {
        Answer answer1 = new Answer();
        answer1.setAnswer("Answer 1");

        Answer answer2 = new Answer();
        answer2.setAnswer("Answer 2");

        Question question = new Question();
        question.setQuestion("Test question");

        questionService.deleteQuestionWithAnswers(question, java.util.List.of(answer1, answer2));

        verify(answerRepository, times(1)).deleteAll(java.util.List.of(answer1, answer2));
        verify(questionRepository, times(1)).delete(question);
    }

    @Test
    void testGetAllQuestions() {
        Question question1 = new Question();
        question1.setQuestion("Question 1");

        Question question2 = new Question();
        question2.setQuestion("Question 2");

        when(questionRepository.findAll()).thenReturn(java.util.List.of(question1, question2));

        assertEquals(2, questionService.getAllQuestions().size());

        verify(questionRepository, times(1)).findAll();
    }


    @Test
    void testGetQuestionsWithAnswersByTrivioId() {
        // Given
        long trivioId = 1L;

        // Create questions
        Question question1 = new Question();
        question1.setId(1L);
        question1.setQuestion("Question 1");

        Question question2 = new Question();
        question2.setId(2L);
        question2.setQuestion("Question 2");

        // Create answers
        Answer answer1 = new Answer();
        answer1.setId(1L);
        answer1.setAnswer("Answer 1");

        Answer answer2 = new Answer();
        answer2.setId(2L);
        answer2.setAnswer("Answer 2");

        List<Answer> answers1 = new ArrayList<>();
        answers1.add(answer1);

        List<Answer> answers2 = new ArrayList<>();
        answers2.add(answer2);

        // Mock repository behavior
        when(questionRepository.findQuestionByTrivioId(trivioId)).thenReturn(List.of(question1, question2));
        when(answerRepository.findAnswersByQuestionId(1L)).thenReturn(answers1);
        when(answerRepository.findAnswersByQuestionId(2L)).thenReturn(answers2);

        // When
        List<QuestionWithAnswers> result = questionService.getQuestionsWithAnswersByTrivioId(trivioId);

        // Then
        assertEquals(2, result.size());
        assertEquals("Question 1", result.get(0).getQuestion().getQuestion());
        assertEquals(1, result.get(0).getAnswers().size());
        assertEquals("Answer 1", result.get(0).getAnswers().get(0).getAnswer());
        assertEquals("Question 2", result.get(1).getQuestion().getQuestion());
        assertEquals(1, result.get(1).getAnswers().size());
        assertEquals("Answer 2", result.get(1).getAnswers().get(0).getAnswer());
    }

    @Test
    void testUpdateQuestionsAndAnswers() {
        // Given
        Question question1 = new Question();
        question1.setId(1L);
        question1.setQuestionType("Multiple Choice");
        question1.setQuestion("Question 1");
        List<String> tags1 = new ArrayList<>();
        tags1.add("tag1");
        question1.setTags(tags1);

        Question question2 = new Question();
        question2.setId(2L);
        question2.setQuestionType("True or False");
        question2.setQuestion("Question 2");
        List<String> tags2 = new ArrayList<>();
        tags2.add("tag2");
        question2.setTags(tags2);

        Answer answer1 = new Answer();
        answer1.setId(1L);
        answer1.setAnswer("Answer 1");
        answer1.setCorrect(true);

        Answer answer2 = new Answer();
        answer2.setId(2L);
        answer2.setAnswer("Answer 2");
        answer2.setCorrect(false);

        QuestionWithAnswers newQuestionWithAnswers1 = new QuestionWithAnswers();
        newQuestionWithAnswers1.setQuestion(question1);
        newQuestionWithAnswers1.setAnswers(List.of(answer1));

        QuestionWithAnswers newQuestionWithAnswers2 = new QuestionWithAnswers();
        newQuestionWithAnswers2.setQuestion(question2);
        newQuestionWithAnswers2.setAnswers(List.of(answer2));

        List<QuestionWithAnswers> newQuestionsWithAnswers = List.of(newQuestionWithAnswers1, newQuestionWithAnswers2);

        List<QuestionWithAnswers> existingQuestionsWithAnswers = List.of(newQuestionWithAnswers1, newQuestionWithAnswers2);

        // Mock repository behavior


        // When
        questionService.updateQuestionsAndAnswers(newQuestionsWithAnswers, existingQuestionsWithAnswers);

        // Then
        verify(questionRepository).save(question1);
        verify(questionRepository).save(question2);
        assertEquals(existingQuestionsWithAnswers.get(0).getQuestion(), newQuestionsWithAnswers.get(0).getQuestion());
        assertEquals(existingQuestionsWithAnswers.get(1).getQuestion(), newQuestionsWithAnswers.get(1).getQuestion());

    }
}
