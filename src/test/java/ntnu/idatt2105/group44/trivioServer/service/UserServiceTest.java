package ntnu.idatt2105.group44.trivioServer.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import ntnu.idatt2105.group44.trivioServer.dto.SignUpRequest;
import ntnu.idatt2105.group44.trivioServer.exception.UserAlreadyExistException;
import ntnu.idatt2105.group44.trivioServer.exception.UserNotFoundException;
import ntnu.idatt2105.group44.trivioServer.model.Role;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ntnu.idatt2105.group44.trivioServer.model.User;
import ntnu.idatt2105.group44.trivioServer.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Test
    void testGetUserById() {
        User user = new User();
        user.setId(5L);
        user.setUsername("Mary");
        when(userRepository.findById(5L)).thenReturn(java.util.Optional.of(user));

        User foundUser = userService.getUserById(5L);
        assertEquals("Mary", foundUser.getUsername());
    }

    @Test
    void testGetUserByUsername() {
        User user = new User();
        user.setId(5L);
        user.setUsername("Mary");
        when(userRepository.findUserByUsername("Mary")).thenReturn(java.util.Optional.of(user));

        User foundUser = userService.getUserByUsername("Mary");
        assertEquals(5L, foundUser.getId());
    }

    @Test
    void testGetAllUsers() {
        User user1 = new User();
        user1.setUsername("Mary");

        User user2 = new User();
        user2.setUsername("John");
        when(userRepository.findAll()).thenReturn(java.util.List.of(user1, user2));

        assertEquals(2, userService.getAllUsers().size());
    }

    @Test
    void testCheckIfUsernameIsPresent() {
        User user = new User();
        user.setUsername("Mary");
        when(userRepository.findUserByUsername("Mary")).thenReturn(java.util.Optional.of(user));

        boolean isPresent = userService.checkIfUsernameIsPresent("Mary");
        assertTrue(isPresent);

        boolean isNotPresent = userService.checkIfUsernameIsPresent("Belle");
        assertFalse(isNotPresent);
    }

    @Test
    void testCheckIfEmailIsPresent() {
        User user = new User();
        user.setEmail("mary@email.com");
        when(userRepository.findUserByEmail("mary@email.com")).thenReturn(
            java.util.Optional.of(user));

        boolean isPresent = userService.checkIfEmailIsPresent("mary@email.com");
        assertTrue(isPresent);

        boolean isNotPresent = userService.checkIfEmailIsPresent("null@email.com");
        assertFalse(isNotPresent);
    }

    @Test
    void testCreateUser() {
        // Mock data
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setUsername("testuser");
        signUpRequest.setEmail("test@example.com");
        signUpRequest.setPassword("password");

        userService.createUser(signUpRequest);
        verify(userRepository).save(any(User.class));

    }

    @Test
    void testUpdateUser() {
        // Mock data
        Long userId = 1L;
        String newUsername = "newUsername";
        String newEmail = "newemail@example.com";
        User user = new User();
        user.setId(userId);
        user.setUsername("oldUsername");
        user.setEmail("oldemail@example.com");
        user.setPassword("oldPassword");
        user.setRole(Role.USER);

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(userRepository.findUserByUsername(newUsername)).thenReturn(Optional.empty());
        when(userRepository.findUserByEmail(newEmail)).thenReturn(Optional.empty());

        userService.updateUser(newUsername, newEmail, userId);

        verify(userRepository).save(user);

        assertEquals(newUsername, user.getUsername());
        assertEquals(newEmail, user.getEmail());
    }

    @Test
    void testUserNotFound() {
        // Mock data
        Long userId = 1L;
        String username = "username";
        String mail = "email@example.com";

        when(userRepository.findById(userId)).thenReturn(Optional.empty());
        // should throw when the user
        assertThrows(
            UserNotFoundException.class, () -> userService.updateUser(username, mail, userId));
    }

    @Test
    void testAttemptToUpdateUsernameWithExistingUsername() {
        // Mock data
        Long userId = 1L;
        String existingUsername = "existingUsername";
        User user = new User();
        user.setId(userId);
        user.setUsername("oldUsername");
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(userRepository.findUserByUsername(existingUsername)).thenReturn(
            Optional.of(new User()));

        assertThrows(UserAlreadyExistException.class, () -> {
            userService.updateUser(existingUsername, null, userId);
        });

        assertEquals("oldUsername", user.getUsername());
    }

    @Test
    void testAttemptToUpdateEmailWithExistingEmail() {
        // Mock data
        Long userId = 1L;
        String existingEmail = "existingEmail@example.com";
        User user = new User();
        user.setId(userId);
        user.setEmail("oldEmail@example.com");
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(userRepository.findUserByEmail(existingEmail)).thenReturn(Optional.of(new User()));

        assertThrows(UserAlreadyExistException.class, () -> {
            userService.updateUser(null, existingEmail, userId);
        });

        assertEquals("oldEmail@example.com", user.getEmail());
    }

    @Test
    void testUpdatePasswordSuccessfully() {
        // Mock data
        Long userId = 1L;
        String newPassword = "newPassword";
        User user = new User();
        user.setId(userId);
        user.setPassword("oldPassword");
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        userService.updatePassword(newPassword, userId);

        assertEquals(newPassword, user.getPassword());
        verify(userRepository).save(user);
    }

    @Test
    void testUpdatePasswordWithNullPassword() {
        // Mock data
        Long userId = 1L;
        User user = new User();
        user.setId(userId);
        user.setPassword("oldPassword");
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        userService.updatePassword(null, userId);

        // Verify that the password remains unchanged
        assertEquals("oldPassword", user.getPassword());
        verify(userRepository).save(user);
    }

    @Test
    void testUpdatePasswordWithEmptyPassword() {
        // Mock data
        Long userId = 1L;
        User user = new User();
        user.setId(userId);
        user.setPassword("oldPassword");
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        userService.updatePassword("", userId);

        assertEquals("oldPassword", user.getPassword());
        verify(userRepository).save(user);
    }

    @Test
    void testUpdatePasswordWithSamePassword() {
        // Mock data
        Long userId = 1L;
        String password = "password";
        User user = new User();
        user.setId(userId);
        user.setPassword(password);
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        userService.updatePassword(password, userId);

        assertEquals(password, user.getPassword());
        verify(userRepository).save(user);
    }
    @Test
    void testUpdatePasswordWithNonExistingUser() {
        Long userId = 1L;
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        assertThrows(UserNotFoundException.class, () -> {
            userService.updatePassword("newPassword", userId);
        });
    }


}