package ntnu.idatt2105.group44.trivioServer.service;

import ntnu.idatt2105.group44.trivioServer.model.Message;
import ntnu.idatt2105.group44.trivioServer.repository.MessageRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MessageServiceTest {

    @Mock
    private MessageRepository messageRepository;

    @InjectMocks
    private MessageService messageService;

    @Test
    void testCreateMessage() {
        Message message = new Message();
        message.setContent("Test message");

        when(messageRepository.save(message)).thenReturn(message);

        Message createdMessage = messageService.createMessage(message);

        assertEquals("Test message", createdMessage.getContent());

        verify(messageRepository, times(1)).save(message);
    }

    @Test
    void testGetAllMessages() {
        Message message1 = new Message();
        message1.setContent("Message 1");

        Message message2 = new Message();
        message2.setContent("Message 2");

        List<Message> messages = new ArrayList<>();
        messages.add(message1);
        messages.add(message2);

        when(messageRepository.findAll()).thenReturn(messages);

        List<Message> returnedMessages = messageService.getAllMessages();

        assertEquals(2, returnedMessages.size());

        verify(messageRepository, times(1)).findAll();
    }
}
