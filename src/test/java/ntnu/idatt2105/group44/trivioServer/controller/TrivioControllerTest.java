package ntnu.idatt2105.group44.trivioServer.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import ntnu.idatt2105.group44.trivioServer.dto.TrivioWithQAndA;
import ntnu.idatt2105.group44.trivioServer.model.Role;
import ntnu.idatt2105.group44.trivioServer.model.Trivio;
import ntnu.idatt2105.group44.trivioServer.model.User;
import ntnu.idatt2105.group44.trivioServer.service.JWTService;
import ntnu.idatt2105.group44.trivioServer.service.TrivioService;
import ntnu.idatt2105.group44.trivioServer.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(TrivioController.class)
class TrivioControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private TrivioService trivioService;

  @MockBean
  private JWTService jwtService;

  @MockBean
  private UserService userService;
  @Autowired
  private ObjectMapper objectMapper;

  @BeforeEach
  void setUp() {
  }

  @Test
  @WithMockUser(roles = "ADMIN")
  void testGetAllTrivios() throws Exception {
    // Mocking the service response
    when(trivioService.getAllTrivios()).thenReturn(Collections.singletonList(new Trivio()));

    // Performing the GET request and asserting the response
    mockMvc.perform(MockMvcRequestBuilders.get("/trivios"))
        .andExpect(status().isOk());
  }

  @Test
  @WithMockUser(roles = "USER")
  void testGetTrivio_Success() throws Exception {
    User user = new User.Builder().username("tst").password("taa").build();
    Trivio trivio = new Trivio();
    trivio.setVisibility("public");
    trivio.setUser(user);
    user.setId(1L);
    trivio.setId(1L);
    when(jwtService.extractSubjectFromHeader(any())).thenReturn("1");
    when(trivioService.getTrivioById(anyLong())).thenReturn(trivio);
    // Performing the GET request and asserting the response
    mockMvc.perform(MockMvcRequestBuilders.get("/trivios/1")
            .header(HttpHeaders.AUTHORIZATION, "Bearer test-token")
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
  }@Test
  @WithMockUser(roles = "USER")
  void testGetTrivio_Failed() throws Exception {
    User user = new User.Builder().username("tst").password("taa").build();
    Trivio trivio = new Trivio();
    trivio.setVisibility("public");
    trivio.setUser(user);
    when(jwtService.extractSubjectFromHeader(any())).thenReturn("1");
    when(trivioService.getTrivioById(anyLong())).thenReturn(trivio);
    // Performing the GET request and asserting the response
    mockMvc.perform(MockMvcRequestBuilders.get("/trivios/1")
            .header(HttpHeaders.AUTHORIZATION, "Bearer test-token")
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser
  void testGetTrivio_InvalidTrivioId() throws Exception {
    when(jwtService.extractSubjectFromHeader(any())).thenReturn("1");
    when(trivioService.getTrivioById(anyLong())).thenReturn(null);

    mockMvc.perform(MockMvcRequestBuilders.get("/trivios/999")
            .header(HttpHeaders.AUTHORIZATION, "Bearer test-token")
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isUnauthorized());
  }
  @Test
  @WithMockUser(roles = "USER")
  void testGetFilteredTriviosByUser_Success() throws Exception {
    // Mocking necessary parameters and service response
    String token = "test-token";
    String category = "history";
    String difficulty = "easy";
    String tagString = "tag1,tag2";
    Pageable pageable = PageRequest.of(0, 10);
    Page<Trivio> trivios = new PageImpl<>(Collections.singletonList(new Trivio()));

    when(jwtService.extractSubjectFromHeader(any())).thenReturn("1");
    when(trivioService.getFilteredTriviosByUser(anyLong(), eq(category), eq(difficulty), eq(Arrays.asList(tagString.split(","))), eq(pageable))).thenReturn(trivios);


    // Performing the GET request and asserting the response
    mockMvc.perform(MockMvcRequestBuilders.get("/trivios/user")
            .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
            .param("category", category)
            .param("difficulty", difficulty)
            .param("tagString", tagString)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  @WithMockUser(roles = "USER")
  void testGetFilteredPublicTriviosByOtherUsers_Success() throws Exception {
    String token = "test-token";
    String category = "history";
    String difficulty = "easy";
    String tagString = "tag1,tag2";
    Pageable pageable = PageRequest.of(0, 10);
    Page<Trivio> trivios = new PageImpl<>(Collections.singletonList(new Trivio()));

    when(jwtService.extractSubjectFromHeader(any())).thenReturn("1");
    when(trivioService.getFilteredPublicTriviosByOtherUsers(anyLong(), eq(category), eq(difficulty), eq(Arrays.asList(tagString.split(","))), eq("public"), eq(pageable))).thenReturn(trivios);

    mockMvc.perform(MockMvcRequestBuilders.get("/trivios/discover")
            .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
            .param("category", category)
            .param("difficulty", difficulty)
            .param("tagString", tagString)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }
  @Test
  @WithMockUser(roles = "USER")
  void testAddTrivioWithQuestionsAndAnswers_Success() throws Exception {
    // Mocking necessary parameters and service response
    User user = new User.Builder().username("tst").password("taa").build();
    user.setId(1L);
    String token = jwtService.generateToken("1");
    when(jwtService.extractSubjectFromHeader(any())).thenReturn("1");
    when(userService.getUserById(any())).thenReturn(user);
    TrivioWithQAndA trivioWithQAndA = new TrivioWithQAndA(); // Populate this object with required data
    doNothing().when(trivioService).createTrivio(eq(trivioWithQAndA), anyLong());

    // Performing the POST request and asserting the response
    mockMvc.perform(MockMvcRequestBuilders.post("/trivios/create")
            .header(HttpHeaders.AUTHORIZATION, "Bearer "+token)
            .content(objectMapper.writeValueAsString(trivioWithQAndA))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.content().string("Trivio Created!"));
  }

  @Test
  @WithMockUser(roles = "USER")
  void testDeleteTrivio_Success() throws Exception {
    // Mocking necessary parameters and service response
    Long trivioId = 1L;
    String token = "dummyToken";
    doNothing().when(trivioService).deleteTrivio(eq(trivioId));
    when(trivioService.checkIfUserIsOwner(any(), any())).thenReturn(true);

    // Performing the DELETE request and asserting the response
    mockMvc.perform(MockMvcRequestBuilders.delete("/trivios/delete/{id}", trivioId)
            .contentType(MediaType.APPLICATION_JSON)
            .header(HttpHeaders.AUTHORIZATION, "Bearer "+token))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.content().string("Trivio successfully removed!"));

  }

  @Test
  @WithMockUser(roles = "USER")
  void testDeleteTrivio_Fail() throws Exception {
    // Mocking necessary parameters and service response
    Long trivioId = 1L;
    String token = "dummyToken";
    doNothing().when(trivioService).deleteTrivio(eq(trivioId));
    when(trivioService.checkIfUserIsOwner(any(), any())).thenReturn(false);

    // Performing the DELETE request and asserting the response
    mockMvc.perform(MockMvcRequestBuilders.delete("/trivios/delete/{id}", trivioId)
            .contentType(MediaType.APPLICATION_JSON)
            .header(HttpHeaders.AUTHORIZATION, "Bearer "+token))
        .andExpect(status().isUnauthorized())
        .andExpect(MockMvcResultMatchers.content().string("User is not authorized!"));
  }

  @Test
  @WithMockUser(roles = "USER")
  void testDeleteTrivio_Exception() throws Exception {
    // Mocking necessary parameters and service response
    Long trivioId = 1L;
    String token = "dummyToken";
    when(trivioService.checkIfUserIsOwner(eq(token), eq(trivioId))).thenThrow(new RuntimeException("Dummy error message"));
    // Performing the DELETE request and asserting the response
    mockMvc.perform(MockMvcRequestBuilders.delete("/trivios/delete/{id}", trivioId)
            .contentType(MediaType.APPLICATION_JSON)
            .header(HttpHeaders.AUTHORIZATION, "Bearer "+token))
        .andExpect(status().isUnauthorized())
        .andExpect(MockMvcResultMatchers.content().string("User is not authorized!"));
  }

  @Test
  @WithMockUser(roles = "USER")
  void testEditTrivioWithQuestionsAndAnswers_Success() throws Exception {
    // Mocking necessary parameters and service response
    Long trivioId = 1L;
    Trivio trivio = new Trivio();
    User user = new User();
    trivio.setUser(user);
    user.setId(1L);
    String token = "dummyToken";
    TrivioWithQAndA updatedTrivioWithQuestionsAndAnswer = new TrivioWithQAndA();
    when(jwtService.extractSubjectFromHeader(any())).thenReturn("1");
    when(trivioService.getTrivioById(any())).thenReturn(trivio);
    doNothing().when(trivioService).editTrivio(eq(updatedTrivioWithQuestionsAndAnswer), eq(trivioId));
    // Performing the PUT request and asserting the response
    mockMvc.perform(MockMvcRequestBuilders.put("/trivios/edit/{trivioId}", trivioId)
            .header("Authorization", token)
            .content(objectMapper.writeValueAsString(updatedTrivioWithQuestionsAndAnswer))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.content().string("Trivio Updated!"));
  }

  @Test
  @WithMockUser(roles = "USER")
  void testEditTrivioWithQuestionsAndAnswers_Unauthorized() throws Exception {
    // Mocking necessary parameters and service response
    Long trivioId = 1L;
    String token = "dummyToken";
    User user = new User();
    user.setId(8L);
    Trivio trivio = new Trivio();
    trivio.setUser(user);
    when(jwtService.extractSubjectFromHeader(any())).thenReturn("1");
    when(trivioService.getTrivioById(any())).thenReturn(trivio);
    TrivioWithQAndA updatedTrivioWithQuestionsAndAnswer = new TrivioWithQAndA(); // Populate this object with required data
    when(trivioService.isUserInListCanEdit(any(Trivio.class), anyLong())).thenReturn(false);

    // Performing the PUT request and asserting the response
    mockMvc.perform(MockMvcRequestBuilders.put("/trivios/edit/{trivioId}", trivioId)
            .header("Authorization", token)
            .content(objectMapper.writeValueAsString(updatedTrivioWithQuestionsAndAnswer))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isUnauthorized());

    // Verifying that the editTrivio method of trivioService is not called since the user is not authorized
    verify(trivioService, never()).editTrivio(any(TrivioWithQAndA.class), eq(trivioId));
  }

  @Test
  @WithMockUser(roles = "USER")
  void testEditTrivioWithQuestionsAndAnswers_BadRequest() throws Exception {
    // Mocking necessary parameters and service response
    Long trivioId = 1L;
    String token = "dummyToken";
    Trivio trivio = new Trivio();
    User user = new User();
    user.setId(1L);
    trivio.setUser(user);
    TrivioWithQAndA updatedTrivioWithQuestionsAndAnswer = new TrivioWithQAndA(); // Populate this object with required data
    String errorMessage = "An error occurred while updating trivio: Dummy error message";
    when(jwtService.extractSubjectFromHeader(any())).thenReturn("1");
    when(trivioService.getTrivioById(any())).thenReturn(trivio);
    doThrow(new RuntimeException()).when(trivioService).editTrivio(any(TrivioWithQAndA.class),any(Long.class));
    // Performing the PUT request and asserting the response
    mockMvc.perform(MockMvcRequestBuilders.put("/trivios/edit/{trivioId}", trivioId)
            .header("Authorization", token)
            .content(objectMapper.writeValueAsString(updatedTrivioWithQuestionsAndAnswer))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }


  @Test
  void testGetSharedTrivios_Success() throws Exception {
    // Mocking necessary parameters and service response
    String token = "test-token";
    String category = "history";
    String difficulty = "easy";
    String tagString = "tag1,tag2";
    PageRequest pageable = PageRequest.of(0, 10);
    List<Trivio> trivioList = Collections.singletonList(new Trivio());
    Page<Trivio> trivios = new PageImpl<>(trivioList);

    when(jwtService.extractSubjectFromHeader(any())).thenReturn("1");
    when(trivioService.getFilteredSharedTriviosByUser(anyLong(), eq(category), eq(difficulty),
        eq(Arrays.asList(tagString.split(","))), eq(pageable))).thenReturn(trivios);

    // Performing the GET request and asserting the response
    mockMvc.perform(buildGetRequest("/trivios/shared", token, category, difficulty, tagString)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  private MockHttpServletRequestBuilder buildGetRequest(String url, String token, String category,
      String difficulty, String tagString) {
    MockHttpServletRequestBuilder requestBuilder =MockMvcRequestBuilders.get(url)
        .header("Authorization", "Bearer " + token);
    if (category != null) {
      requestBuilder.param("category", category);
    }
    if (difficulty != null) {
      requestBuilder.param("difficulty", difficulty);
    }
    if (tagString != null) {
      requestBuilder.param("tagString", tagString);
    }
    return requestBuilder;
  }
}