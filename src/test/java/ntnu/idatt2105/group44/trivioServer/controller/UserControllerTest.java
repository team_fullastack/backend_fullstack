package ntnu.idatt2105.group44.trivioServer.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import ntnu.idatt2105.group44.trivioServer.config.SecurityConfig;
import ntnu.idatt2105.group44.trivioServer.model.Role;
import ntnu.idatt2105.group44.trivioServer.model.User;
import ntnu.idatt2105.group44.trivioServer.service.JWTService;
import ntnu.idatt2105.group44.trivioServer.service.UserService;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest( UserController.class)
public class UserControllerTest {
@Autowired
  private MockMvc mockMvc;

  @MockBean
  private UserService userService;

  @MockBean
  private JWTService jwtService;

  @Autowired
  private UserController userController;

  @Test
  @WithMockUser(roles = "ADMIN")
   void testGetUsers_Success() throws Exception {
    List<User> users = new ArrayList<>();
    User user1 = new User.Builder().username("johndoe").email("email@mail.com").build();
    User user2 = new User.Builder().username("janedoe").email("email2@mail.com").build();
    users.add(user1);
    users.add(user2);

    // Mock the behavior of userService.getAllUsers method
    when(userService.getAllUsers()).thenReturn(users);


    // Act and assert
    mockMvc.perform(MockMvcRequestBuilders.get("/users/all"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].username").value("johndoe"))
        .andExpect(MockMvcResultMatchers.jsonPath("$[1].username").value("janedoe"));
    verify(userService, times(1)).getAllUsers();
  }

  @Test
  @WithMockUser
  void testGetUserInfo_Success() throws Exception {
    // Arrange
    User user = new User.Builder().username("testuser").email("testuser@example.com").role(Role.USER).build();
    String token = jwtService.generateToken("1");
    user.setId(1L);
    when(jwtService.extractSubjectFromHeader(any())).thenReturn("1");
    when(userService.getUserById(1L)).thenReturn(user);

    // Act & Assert
    mockMvc.perform(MockMvcRequestBuilders.get("/users")
            .header("Authorization", "Bearer "+token))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
        .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("testuser"))
        .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("testuser@example.com"));

    verify(userService, times(1)).getUserById(1L);
  }
  @Test
  @WithMockUser
  void testGetOwnInfo_Success() throws Exception {
    // Arrange
    User user = new User.Builder().username("testuser").email("testuser@example.com").role(Role.USER).build();
    String token = jwtService.generateToken("1");
    user.setId(1L);

    // Mocking service behavior
    when(jwtService.extractSubjectFromHeader(any())).thenReturn("1");
    when(userService.getUserById(1L)).thenReturn(user);

    // Performing the GET request and asserting the response
    mockMvc.perform(MockMvcRequestBuilders.get("/users")
            .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
        .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("testuser"))
        .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("testuser@example.com"));
  }
  @WithMockUser
  @Test
  void testUpdateUserInfo_Success() throws Exception {
    // Arrange
    String token = jwtService.generateToken("1");
    when(jwtService.extractSubjectFromHeader(any())).thenReturn("1"); // Mocking the token extraction to return a valid string

    mockMvc.perform(MockMvcRequestBuilders.put("/users/edit")
            .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().string("Username or email successfully changed"));
  }

  @Test
  @WithMockUser
  void testUpdatePassword_Success() throws Exception {
    // Arrange
    String token = jwtService.generateToken("1");
    when(jwtService.extractSubjectFromHeader(any())).thenReturn("1"); // Mocking the token extraction to return a valid string

    mockMvc.perform(MockMvcRequestBuilders.put("/users/editPassword")
            .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
            .contentType(MediaType.APPLICATION_JSON)
            .param("password", "password"))
        .andExpect(status().isOk())
        .andExpect(content().string("Password has successfully been changed!"));
  }

}
