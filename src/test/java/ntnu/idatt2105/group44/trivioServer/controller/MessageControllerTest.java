package ntnu.idatt2105.group44.trivioServer.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import ntnu.idatt2105.group44.trivioServer.dto.UserResponse;
import ntnu.idatt2105.group44.trivioServer.model.Message;
import ntnu.idatt2105.group44.trivioServer.service.JWTService;
import ntnu.idatt2105.group44.trivioServer.service.MessageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(MessageController.class)
public class MessageControllerTest {
  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private MessageService messageService;

//  @MockBean
//  private JWTService jwtService;

  @Autowired
  private MessageController messageController;

  private ObjectMapper objectMapper;

  @BeforeEach
  void setUp() {
    objectMapper = new ObjectMapper();
  }
  @Test
  @WithMockUser
  void testCreateMessage_Success() throws Exception {
    Message message = new Message();
    message.setContent("Hello World");
    message.setSenderEmail("johndoe@email.com");
    message.setSenderName("John Doe");

    String messageJson = objectMapper.writeValueAsString(message);

    mockMvc.perform(MockMvcRequestBuilders.post("/messages")
        .contentType(MediaType.APPLICATION_JSON)
        .content(messageJson)
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  @WithMockUser
  void testCreateMessage_Failure() throws Exception {
    // Arrange
    Message message = new Message();
    message.setContent("Hello World");
    message.setSenderEmail("johndoe@email.com");
    message.setSenderName("John Doe");

    when(messageService.createMessage(message)).thenThrow(new RuntimeException("Failed to create message"));

    // Act
    mockMvc.perform(MockMvcRequestBuilders.post("/messages")
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }


  @Test
  @WithMockUser
  void testGetAllMessages_Success() throws Exception {
    // Arrange
    List<Message> messages = new ArrayList<>();
    messages.add(new Message("John", "johndoe@email.com", "Hello"));
    messages.add(new Message("Jane", "janedoe@email.com", "Hi"));

    when(messageService.getAllMessages()).thenReturn(messages);

    // Act & Assert
    mockMvc.perform(MockMvcRequestBuilders.get("/messages"))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(messages.size()))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].content").value("Hello"))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].senderName").value("John"))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].senderEmail").value("johndoe@email.com"))
        .andExpect(MockMvcResultMatchers.jsonPath("$[1].content").value("Hi"))
        .andExpect(MockMvcResultMatchers.jsonPath("$[1].senderName").value("Jane"))
        .andExpect(MockMvcResultMatchers.jsonPath("$[1].senderEmail").value("janedoe@email.com"));
  }
}