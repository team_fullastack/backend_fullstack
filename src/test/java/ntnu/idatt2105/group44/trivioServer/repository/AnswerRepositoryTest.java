package ntnu.idatt2105.group44.trivioServer.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import ntnu.idatt2105.group44.trivioServer.model.Answer;
import ntnu.idatt2105.group44.trivioServer.model.Question;

@DataJpaTest
class AnswerRepositoryTest {
    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Test
    void testFindAnswersByQuestionId() {
        Question question1 = new Question();
        question1.setQuestion("What is the capital of Norway?");
        questionRepository.save(question1);

        Answer answer1 = new Answer();
        answer1.setAnswer("Oslo");
        answer1.setQuestion(question1);
        answerRepository.save(answer1);

        Answer answer2 = new Answer();
        answer2.setAnswer("Stockholm");
        answer2.setQuestion(question1);
        answerRepository.save(answer2);

        assertEquals(2, answerRepository.findAnswersByQuestionId(question1.getQuestionId()).size());
    }

    @Test
    void testDeleteAll() {
        Question question1 = new Question();
        question1.setQuestion("What is the capital of Norway?");
        questionRepository.save(question1);

        Answer answer1 = new Answer();
        answer1.setAnswer("Oslo");
        answer1.setQuestion(question1);
        answerRepository.save(answer1);

        Answer answer2 = new Answer();
        answer2.setAnswer("Stockholm");
        answer2.setQuestion(question1);
        answerRepository.save(answer2);

        answerRepository.deleteAll();

        assertEquals(0, answerRepository.findAnswersByQuestionId(question1.getQuestionId()).size());
    }
}
