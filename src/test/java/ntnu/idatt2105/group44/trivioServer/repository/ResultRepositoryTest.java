package ntnu.idatt2105.group44.trivioServer.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.DirtiesContext;

import ntnu.idatt2105.group44.trivioServer.model.Result;
import ntnu.idatt2105.group44.trivioServer.model.Trivio;
import ntnu.idatt2105.group44.trivioServer.model.User;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

@DataJpaTest
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
class ResultRepositoryTest {
    @Autowired
    private ResultRepository resultRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TrivioRepository trivioRepository;

    @Test
    void testGetResultsByUserId() {
        User user = new User();
        user.setId(1L);
        userRepository.save(user);

        Trivio trivio1 = new Trivio();
        trivio1.setUser(user);
        trivioRepository.save(trivio1);

        Result result1 = new Result();
        result1.setUser(user);
        result1.setTrivio(trivio1);
        resultRepository.save(result1);

        Result result2 = new Result();
        result2.setUser(user);
        result2.setTrivio(trivio1);
        resultRepository.save(result2);

        Pageable pageable = PageRequest.of(0, 10);

        Page<Result> results = resultRepository.getResultsByUserId(1L, pageable);

        assertEquals(2, results.getTotalElements());
    }

    @Test
    void testGetResultsByUserIdAndTrivio_Title() {
        User user = new User();
        user.setId(1L);
        userRepository.save(user);

        Trivio trivio = new Trivio();
        trivio.setUser(user);
        trivio.setTitle("Test Trivio");
        trivioRepository.save(trivio);

        Result result = new Result();
        result.setUser(user);
        result.setTrivio(trivio);
        resultRepository.save(result);

        Pageable pageable = PageRequest.of(0, 10);

        Page<Result> results = resultRepository.getResultsByUserIdAndTrivio_Title(1L, "Test Trivio", pageable);

        assertEquals(1, results.getTotalElements());
    }

    @Test
    void testFindDistinctTrivioTitlesByUserId() {
        User user = new User();
        user.setId(1L);
        userRepository.save(user);

        Trivio trivio1 = new Trivio();
        trivio1.setUser(user);
        trivio1.setTitle("Trivio 1");
        trivioRepository.save(trivio1);

        Trivio trivio2 = new Trivio();
        trivio2.setUser(user);
        trivio2.setTitle("Trivio 2");
        trivioRepository.save(trivio2);

        Result result1 = new Result();
        result1.setUser(user);
        result1.setTrivio(trivio1);
        resultRepository.save(result1);

        Result result2 = new Result();
        result2.setUser(user);
        result2.setTrivio(trivio2);
        resultRepository.save(result2);

        List<String> titles = resultRepository.findDistinctTrivioTitlesByUserId(1L);

        assertEquals(2, titles.size());
    }

}