package ntnu.idatt2105.group44.trivioServer.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import ntnu.idatt2105.group44.trivioServer.model.Question;
import ntnu.idatt2105.group44.trivioServer.model.Trivio;

@DataJpaTest
class QuestionRepositoryTest {
    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private TrivioRepository trivioRepository;

    @Test
    void testFindQuestionByTrivioId() {
        Trivio trivio1 = new Trivio();
        trivioRepository.save(trivio1);

        Question question1 = new Question();
        question1.setQuestion("What is the capital of Norway?");
        question1.setTrivio(trivio1);
        questionRepository.save(question1);

        Question question2 = new Question();
        question2.setTrivio(trivio1);
        question2.setQuestion("What is the capital of Sweden?");
        questionRepository.save(question2);

        assertEquals(2, questionRepository.findQuestionByTrivioId(trivio1.getId()).size());
    }
}
