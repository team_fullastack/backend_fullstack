package ntnu.idatt2105.group44.trivioServer.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import ntnu.idatt2105.group44.trivioServer.model.User;
import ntnu.idatt2105.group44.trivioServer.model.Role;

@DataJpaTest
class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    @Test
    void testFindUserByUsername() {
        User user1 = new User("John", "password", "john@email.com", Role.USER);
        userRepository.save(user1);

        Optional<User> user = userRepository.findUserByUsername("John");
        assertEquals("John", user.get().getUsername());
    }   
    
    @Test
    void testFindUserByEmail() {
        User user1 = new User("Jane", "password", "jane@email.com", Role.USER);
        userRepository.save(user1);

        Optional<User> user = userRepository.findUserByEmail("jane@email.com");
        assertEquals("jane@email.com", user.get().getEmail());
    }    
}
