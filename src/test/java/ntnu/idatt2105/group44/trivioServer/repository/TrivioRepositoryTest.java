package ntnu.idatt2105.group44.trivioServer.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import ntnu.idatt2105.group44.trivioServer.model.Trivio;
import ntnu.idatt2105.group44.trivioServer.model.User;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

@DataJpaTest
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
class TrivioRepositoryTest {
    @Autowired
    private TrivioRepository trivioRepository;
    
    @Autowired
    private UserRepository userRepository;

    @Test
    void testFindTrivioByTitle() {
        Trivio trivio1 = new Trivio();
        trivio1.setTitle("Trivio 1");
        trivioRepository.save(trivio1);

        Optional<Trivio> trivio = trivioRepository.findTrivioByTitle("Trivio 1");
        assertEquals("Trivio 1", trivio.get().getTitle());
    }

    @Test 
    void testFindTrivioByVisibility() {
        Trivio trivio1 = new Trivio();
        trivio1.setVisibility("public");
        trivioRepository.save(trivio1);

        Trivio trivio2 = new Trivio();
        trivio2.setVisibility("private");
        trivioRepository.save(trivio2);

        assertEquals(1, trivioRepository.findTrivioByVisibility("public").size());
        assertEquals(1, trivioRepository.findTrivioByVisibility("private").size());
    }

    @Test
    void testFindTrivioByUserId() {
        User user = new User();
        user.setId(1L);
        userRepository.save(user);

        Trivio trivio1 = new Trivio();
        trivio1.setUser(user);
        trivioRepository.save(trivio1);

        Trivio trivio2 = new Trivio();
        trivio2.setUser(user);
        trivioRepository.save(trivio2);

        assertEquals(2, trivioRepository.findTrivioByUserId(1L).size());
    }

    @Test 
    void testFindTrivioByVisibilityAndUserIdNot() {
        User user = new User();
        user.setId(1L);
        userRepository.save(user);

        User user2 = new User();
        user2.setId(2L);
        userRepository.save(user2);

        Trivio trivio1 = new Trivio();
        trivio1.setVisibility("public");
        trivio1.setUser(user2);

        Trivio trivio2 = new Trivio();
        trivio2.setVisibility("public");
        trivio2.setUser(user2);

        trivioRepository.save(trivio1);
        trivioRepository.save(trivio2);

        assertEquals(2, trivioRepository.findTrivioByVisibilityAndUserIdNot("public", 1L).size());
        assertEquals(0, trivioRepository.findTrivioByVisibilityAndUserIdNot("public", 2L).size());
    }
}
