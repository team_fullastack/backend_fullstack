package ntnu.idatt2105.group44.trivioServer.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import ntnu.idatt2105.group44.trivioServer.model.Message;

import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
class MessageRepositoryTest {

    @Autowired
    private MessageRepository messageRepository;

    @Test
    void testFindBySenderName() {
        Message message1 = new Message("John", "john@example.com", "Hello");
        Message message2 = new Message("John", "john@example.com", "Hi");
        Message message3 = new Message("Jane", "jane@example.com", "Hey");

        messageRepository.save(message1);
        messageRepository.save(message2);
        messageRepository.save(message3);

        List<Message> messagesByJohn = messageRepository.findBySenderName("John");
        List<Message> messagesByJane = messageRepository.findBySenderName("Jane");

        assertEquals(2, messagesByJohn.size());
        assertEquals(1, messagesByJane.size());
    }
}
